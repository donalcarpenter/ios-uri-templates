//
//  EZUriCharacterMap.h
//  uriTemplatesHost
//
//  Created by Donal Carpenter on 15/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EZUriCharacterMap : NSObject

- (id) initWithString: (NSString *) string;

- (id) initWithCharCode: (unichar) charCode;

- (void) appendCharCode: (unichar) charCode;

- (NSString *) reservedString;

- (NSString *) string;

- (NSString *) reservedStringWithPrefix: (uint) prefix;

- (NSString *) stringWithPrefix: (uint) prefix;;

@end
