//
//  EZExpressionFactory.h
//  uriTemplates
//
//  Created by Donal Carpenter on 10/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EZExpression.h"

@interface EZExpressionFactory : NSObject

@property (getter = isInExpression) BOOL inExpression;

- (EZExpression *) expressionForExpansion: (NSString *) expansionIdentifier;


@end
