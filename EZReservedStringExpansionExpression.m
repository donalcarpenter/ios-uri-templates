//
//  EZReservedStringExpansionExpression.m
//  uriTemplates
//
//  Created by Donal Carpenter on 10/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import "EZReservedStringExpansionExpression.h"
#import "EZUriCharacterMap.h"
#import "EZUriTemplateVariableExpander.h"

@interface EZReservedStringExpansionExpression() <EZUriTemplateVariableExpander>

@end

@implementation EZReservedStringExpansionExpression

@synthesize delegate, successor;

+ (BOOL)canExpandType:(id)obj{
    return [obj isKindOfClass:[EZUriCharacterMap class]];
}

- (NSString *)expand:(id)obj forSpec:(EZUriTemplateVariableSpec *)spec{
    if(![[self class] canExpandType:obj]){
        self.successor.delegate = self.delegate;
        return [self.successor expand:obj forSpec:spec];
    }
    
    EZUriCharacterMap* map = (EZUriCharacterMap*)obj;
    
    if(spec.modifier != EZUriTemplateVariablePrefixed){
        return [map reservedString];
    }
    
    return [map reservedStringWithPrefix:spec.prefixLength];
}
@end
