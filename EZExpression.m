//
//  EXExpression.m
//  uriTemplates
//
//  Created by Donal Carpenter on 10/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import "EZExpression.h"
#import "EZUriExpandableObject.h"
#import "EZUriTemplateVariableExpander.h"
#import "EZUriTemplateVariableExpanderFactory.h"

@interface EZExpression()<EZUriTemplateVariableExpanderDelegate, EZUriTemplateVariableExpander>

@end

@implementation EZExpression

@synthesize characterMapExpander, expression, delegate, successor;

- (NSArray *)variableSpecs{
    return [self->_variableSpecs copy];
}


- (id)init{
    if(self = [super init]){
        self->_variableSpecs = [[NSMutableArray alloc] init];
        
        self.characterMapExpander = self;
        self.expression = self;
        self.delegate = self;
        
        return  self;
    }
    
    return nil;
}

- (void)addVariableSpec:(EZUriTemplateVariableSpec *)variableSpec{
    [self->_variableSpecs addObject:variableSpec];
}

- (NSString *) expand: (NSDictionary*)variables{
    
    if(!self->_variableSpecs){
        return  nil;
    }
    
    id lastObj = [self->_variableSpecs lastObject];
    if(!lastObj){
        return nil;
    }
    
    // start off a new mutable string with the prepend value
    __block NSMutableString *uri = nil;
    __block NSString *separator = @"";

    
    // check if we have been supplied with a chain of expanders
    if(!self.successor){
        self.successor = [EZUriTemplateVariableExpanderFactory defaultExpanderChain];
    }
    
    
    EZExpression * __weak weakSelf= self;
    [self->_variableSpecs enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {

        EZUriTemplateVariableSpec *spec = obj;
        id var = variables[spec.name];
        
        
        if(!var)
            return;
        
        // expand it here through the chain of successers
        NSString *expanded = [weakSelf expand:var forSpec:spec];
        
        
        if(!expanded)
            return;
        
        // only instantiate uri m  when we definitely have something
        // this is necessary because when there is only undef vars
        // we don't want to show any front appendages
        if(!uri){
            uri = [self.prependor mutableCopy];
        }
        
        // do not add a separator if this element has expanded to nothing
        // unless overridden by specialist expansion
        if(![expanded length] && !weakSelf.applySeparatorWhenExpandedVariableIsEmpty){
            return;
        }
        
        // be careful to only allow 1 trailing separator
        if([uri hasSuffix:separator]){
            return;
        }
        
        [uri appendFormat:@"%@%@", separator, expanded];
        
        // add separator
        separator = [weakSelf separatorForSpec:nil];
    }];
    
    // return the uri, or an empty string if nil
    return uri ? uri : @"";
}

#pragma mark -- to be overridden

- (BOOL)applySeparatorWhenExpandedVariableIsEmpty{
    return NO;
}

-(NSString *)separatorForSpec:(EZUriTemplateVariableSpec *)variableSpec{
    return @",";
}

- (NSString *)keyValueSeparatorForSpec:(EZUriTemplateVariableSpec *)variableSpec{
    NSString *nameValueSeparator = variableSpec.modifier == EZUriTemplateVariableModifierExploded ? @"=" : @",";
    
    return nameValueSeparator;
}

- (NSString *) prependor{
    return @"";
}

#pragma mark EZUriTemplateVariableExpander

+ (BOOL)canExpandType:(id)obj{
    return [obj isKindOfClass:[EZUriCharacterMap class]];
}

- (NSString *)expand:(id)obj forSpec:(EZUriTemplateVariableSpec *)spec{
    if(![[self class] canExpandType:obj])
    {
        self.successor.delegate = self.delegate;
        return [self.successor expand:obj forSpec:spec];
    }
    
    EZUriCharacterMap *map = (EZUriCharacterMap*)obj;
    
    if(spec.modifier != EZUriTemplateVariablePrefixed)
        return [map string];
        
    return [map stringWithPrefix:spec.prefixLength];
}

@end
