//
//  EZUriCharacter.h
//  uriTemplatesHost
//
//  Created by Donal Carpenter on 14/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import <UIKit/UIKit.h>




@interface EZUriCharacter : NSObject

typedef NS_ENUM(NSUInteger, EZUriCharacterClass) {
    EZUriCharacterClassUnreserved,
    EZUriCharacterClassReserved,
    EZUriCharacterClassPctEncoded,
};

+ (EZUriCharacter*)uriCharacterWithCharCode:(unichar)charCode;

@property (readonly) unichar charCode;
@property (readonly) EZUriCharacterClass charClass;

- (NSString*) string;
- (NSString*) reservedString;

-(id) initWithCharCode: (unichar) charCode;
-(id) initWithString: (NSString *) string;
-(id) initWithPctEncodedString: (NSString *) pctEncodedString;

@end
