//
//  EZArrayExpander.m
//  uriTemplatesHost
//
//  Created by Donal Carpenter on 16/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import "EZArrayExpander.h"

@implementation EZArrayExpander

@synthesize delegate, successor;

+ (BOOL)canExpandType:(id)obj{
    return [obj isKindOfClass:[NSArray class]];
}

- (NSString *)expand:(id)obj forSpec:(EZUriTemplateVariableSpec *)spec{
    if(![[self class] canExpandType:obj])
    {
        self.successor.delegate = self.delegate;
        return [self.successor expand:obj forSpec:spec];
    }
    
    __block NSMutableString *uri = [NSMutableString new];
    __block NSString* separtor = @"";

    NSArray* array = (NSArray*) obj;
    

    EZArrayExpander * __weak weakSelf= self;
    [array enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        if(![[weakSelf.delegate.characterMapExpander class] canExpandType:obj]){
            return;
        }
        
        NSString *value = [weakSelf.delegate.characterMapExpander expand:obj forSpec:spec];
        
        [uri appendString:separtor];
        
        [uri appendString:value];
        
        separtor = [weakSelf.delegate.expression separatorForSpec:spec];
    }];
    
    return uri;
}
@end
