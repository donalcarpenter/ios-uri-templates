//
//  EZUriTemplateVariableExpander.h
//  uriTemplatesHost
//
//  Created by Donal Carpenter on 16/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EZUriTemplateVariableSpec.h"
#import "EZExpression.h"

@protocol EZUriTemplateVariableExpander;
@class EZExpression;

@protocol EZUriTemplateVariableExpanderDelegate <NSObject>
@property (weak, nonatomic) EZExpression* expression;
@property (weak, nonatomic) id<EZUriTemplateVariableExpander> characterMapExpander;
@end


@protocol EZUriTemplateVariableExpander <NSObject>

@property (nonatomic, strong) id<EZUriTemplateVariableExpander> successor;

+(BOOL) canExpandType: (id) obj;

@property (weak, nonatomic) id<EZUriTemplateVariableExpanderDelegate> delegate;
-(NSString *) expand: (id) obj forSpec: (EZUriTemplateVariableSpec *) spec;

@end
