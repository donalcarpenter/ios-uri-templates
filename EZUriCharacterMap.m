//
//  EZUriCharacterMap.m
//  uriTemplatesHost
//
//  Created by Donal Carpenter on 15/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import "EZUriCharacterMap.h"
#import "EZUriCharacter.h"

@implementation EZUriCharacterMap
{
    NSMutableString *_res;
    NSMutableString *_unres;
    NSMutableArray *_chars;
}

- (id)init
{
    self = [super init];
    if (self) {
        self->_chars = [NSMutableArray new];
    }
    return self;
}

- (id)initWithCharCode:(unichar)charCode
{
    self = [super init];
    if (self) {
        
        self->_chars = [NSMutableArray arrayWithObject:[EZUriCharacter uriCharacterWithCharCode:charCode]];
        
    }
    return self;
}

- (id)initWithString:(NSString *)string
{
    if(!string){
        return nil;
    }
    
    self = [super init];
    if (self) {
        
        self->_chars = [NSMutableArray new];
        
        int len = [string length];
        
        for(int i = 0; i < len; i++){
            
            unichar c = [string characterAtIndex:i];
            EZUriCharacter *uchar;
            
            
            if(c == '%'){
                // this is pct encoded so get the next 2 chars and convert to unichar
                
                @try {
                    
                    NSRange r = NSMakeRange(i, 3);
                    
                    if(r.location + r.length >= len){
                        uchar = [EZUriCharacter uriCharacterWithCharCode:'%'];
                    }else{
                        NSString *pct = [string substringWithRange:r];
                        
                        // need to be careful not to double encode '%'
                        if([pct isEqualToString: @"%25"]){
                            
                            uchar = [EZUriCharacter uriCharacterWithCharCode:'%'];
                            
                        }else{
                            
                            uchar = [[EZUriCharacter alloc] initWithPctEncodedString:pct];
                            
                        }
                        
                        // move on passed the pct encoded char
                        i+=2;
                    }
                }
                @catch (NSException *exception) {
                    
                    // maybe % was last char in string
                    
                    uchar = [EZUriCharacter uriCharacterWithCharCode:'%'];
                }
            }
            else
            {
                uchar = [EZUriCharacter uriCharacterWithCharCode:c];
            }
        
            [self->_chars addObject:uchar];
        }
    }
    return self;
}

- (void)appendCharCode:(unichar)charCode{
    
    // clear out any previously cached strings
    if(self->_res) {
        self->_res = nil;
    }
    
    if(self->_unres) {
        self->_unres = nil;
    }
    
    [self->_chars addObject:[EZUriCharacter uriCharacterWithCharCode:charCode]];
}

- (NSString *)reservedString{
    if(self->_res){
        return self->_res;
    }
    
    self->_res = [NSMutableString new];
    
    for (EZUriCharacter *c in self->_chars) {
        [self->_res appendString: [c reservedString]];
    }
    
    return self->_res;
}

- (NSString *)string{
    if(self->_unres){
        return self->_unres;
    }
    
    self->_unres = [self desc];
    
    return self->_unres;
}

- (NSString *)description{
    
    return [self desc];
}

- (NSMutableString*) desc{
    NSMutableString *s = [NSMutableString new];
    
    for (EZUriCharacter *c in self->_chars) {
        [s appendString: [c string]];
    }
    
    return s;
}

- (NSString *)stringWithPrefix:(uint)prefix{

    int len = [self->_chars count];
    
    int max = len < prefix ? len : prefix;
    
    NSMutableString *s = [NSMutableString new];
    
    for (int i = 0; i < max; i++) {
        [s appendString: [self->_chars[i] string]];
    }
    
    return s;
}

- (NSString *)reservedStringWithPrefix:(uint)prefix{
    
    int len = [self->_chars count];
    
    int max = len < prefix ? len : prefix;
    
    NSMutableString *s = [NSMutableString new];
    
    for (int i = 0; i < max; i++) {
        [s appendString: [self->_chars[i] reservedString]];
    }
    
    return s;
}

@end
