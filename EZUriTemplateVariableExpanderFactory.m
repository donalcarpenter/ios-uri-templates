//
//  EZUriTemplateVariableExpanderFactory.m
//  uriTemplatesHost
//
//  Created by Donal Carpenter on 16/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import "EZUriTemplateVariableExpanderFactory.h"
#import "EZDictionaryExpander.h"
#import "EZArrayExpander.h"
#import "EZOrderedDictionaryExpander.h"
#import "EZUriTemplateVariableExpander.h"
#import "EZExpandableObjectExpander.h"

@implementation EZUriTemplateVariableExpanderFactory

- (id)initWithCharacterMapExpander:(id<EZUriTemplateVariableExpander>)characterMapExpander
{
    self = [super init];
    if (self) {
        self->_characterMapExpander = characterMapExpander;
    }
    return self;
}

+ (id<EZUriTemplateVariableExpander>) convertExpanderListToChain: (NSArray*) expanders{
    
    id<EZUriTemplateVariableExpander> exp;
    for(int i = [expanders count] - 1; i >= 1; i--){
        exp = expanders[i - 1];
        exp.successor = expanders[i];
    }
    
    return exp;
}

+ (id<EZUriTemplateVariableExpander>)defaultExpanderChain{
    
    NSArray* expanders = @[
                            [EZOrderedDictionaryExpander new],
                            [EZDictionaryExpander new],
                            [EZArrayExpander new],
                            [EZExpandableObjectExpander new]
                            ];
    
    return [self convertExpanderListToChain:expanders];
    
}

- (id<EZUriTemplateVariableExpander>)expanderForObject:(id)obj
{
    if([[self->_characterMapExpander class] canExpandType: obj])
        return self.characterMapExpander;
    
    if([EZOrderedDictionaryExpander canExpandType:obj])
        return [EZOrderedDictionaryExpander new];
    
    if([EZDictionaryExpander canExpandType:obj])
        return [EZDictionaryExpander new];
    
    if([EZArrayExpander canExpandType:obj])
        return [EZArrayExpander new];
    
    if([EZExpandableObjectExpander canExpandType:obj])
        return [EZExpandableObjectExpander new];
    
    return nil;
}
@end
