//
//  EZPathStyleExpansionExtension.m
//  uriTemplatesHost
//
//  Created by Donal Carpenter on 10/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import "EZPathStyleExpansionExpression.h"
#import "EZUriExpandableObject.h"
#import "EZUriTemplateVariableExpander.h"
#import "EZUriTemplateVariableExpanderFactory.h"

#import "EZVarNameAwareArrayExpander.h"
#import "EZVarNameAwareDictionaryExpander.h"
#import "EZVarNameAwareExplodedArrayExpander.h"
#import "EZVarNameAwareExplodedDictionaryExpander.h"
#import "EZExpandableObjectExpander.h"

@interface EZPathStyleExpansionExpression() <EZUriTemplateVariableExpanderDelegate, EZUriTemplateVariableExpander>

@end

@implementation EZPathStyleExpansionExpression

@synthesize characterMapExpander, expression, delegate, successor;



- (NSString *) expand: (NSDictionary*)variables{
    
    if(!_variableSpecs){
        return nil;
    }

    
    id lastObj = [self->_variableSpecs lastObject];
    if(!lastObj){
        return nil;
    }
    
    // start off a new mutable string with the prepend value
    __block NSMutableString *uri = nil;
    __block NSString *separator = @"";
    
    
    if(!self.successor){
        
        
        NSArray *expanders = @[[EZVarNameAwareArrayExpander new],
                               [EZVarNameAwareDictionaryExpander new],
                               [EZVarNameAwareExplodedArrayExpander new],
                               [EZVarNameAwareExplodedDictionaryExpander new],
                               [EZExpandableObjectExpander new]];
        
        self.successor = [EZUriTemplateVariableExpanderFactory convertExpanderListToChain:expanders];
    }
    
    EZPathStyleExpansionExpression * __weak weakSelf = self;
    [self->_variableSpecs enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        EZUriTemplateVariableSpec *spec = obj;
        id var = variables[spec.name];
        
        
        if(!var)
            return;
        
        NSString *expanded = [weakSelf expand:var forSpec:spec];
        
        if(!uri){
            uri = [weakSelf.prependor mutableCopy];
        }
        
        [uri appendFormat:@"%@%@", separator, expanded];
        
        separator = [weakSelf separatorForSpec:spec];
    
    }];
    
    // return the uri, or an empty string if nil
    return uri ? uri : @"";
}

-(NSString *)separatorForSpec:(EZUriTemplateVariableSpec *)variableSpec{
    if(!variableSpec)
        return @";";
    
    return @";";
}

- (NSString *) prependor{
    return @";";
}

#pragma mark EZUriTemplateVariable Expander


+ (BOOL)canExpandType:(id)obj{
    return [obj isKindOfClass:[EZUriCharacterMap class]];
}

- (NSString *)expand:(id)obj forSpec:(EZUriTemplateVariableSpec *)spec{
    if(![[self class] canExpandType:obj])
    {
        self.successor.delegate = self;
        return [self.successor expand:obj forSpec:spec];
    }
    
    EZUriCharacterMap *value = (EZUriCharacterMap*)obj;
    EZUriCharacterMap *name = [[EZUriCharacterMap alloc] initWithString:spec.name];
    
    NSString *v = spec.modifier != EZUriTemplateVariablePrefixed ? [value string] : [value stringWithPrefix:spec.prefixLength];
    NSString *n = [name string];
    
    if(![v length]){
        if(self.applySeparatorWhenExpandedVariableIsEmpty){
            return [NSString stringWithFormat:@"%@=", n];
        }else{
            return n;
        }
    }
    
    if(![n length]){
        return v;
    }
    
    return [NSString stringWithFormat:@"%@=%@", n, v];
}

@end
