//
//  EZPathExpansionExpression.m
//  uriTemplates
//
//  Created by Donal Carpenter on 10/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import "EZPathSegmentExpansionExpression.h"

@implementation EZPathSegmentExpansionExpression

-(NSString *)prependor{
    return @"/";
}

- (NSString *)separatorForSpec:(EZUriTemplateVariableSpec *)variableSpec    {
    
    if(!variableSpec)
        return @"/";
    
    return variableSpec.modifier == EZUriTemplateVariableModifierExploded ? @"/" : @",";
}

- (BOOL)applySeparatorWhenExpandedVariableIsEmpty{
    return YES;
}

@end
