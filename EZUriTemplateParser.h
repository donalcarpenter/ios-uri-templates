//
//  EZUriTemplateParser.h
//  uriTemplates
//
//  Created by Donal Carpenter on 10/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EZExpressionFactory.h"

@interface EZUriTemplateParser : NSObject

@property (strong, nonatomic) EZExpressionFactory* factory;

- (id) initWithTemplate: (NSString *) uriTemplate;

- (NSArray*) expressions: (NSError **) error;

@end
