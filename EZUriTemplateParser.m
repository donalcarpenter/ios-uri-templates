//
//  EZUriTemplateParser.m
//  uriTemplates
//
//  Created by Donal Carpenter on 10/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import "EZUriTemplateParser.h"
#import "EZConstantExpression.h"
#import "EZSimpleStringExpansionExpression.h"

#define PREFIX_MODIFIER_REGEX @":([1-9]{1,1})(\\d{0,4})$"

@interface EZUriTemplateParser()
- (EZUriTemplateVariableSpec*) getVariableFromString: (NSString*) buffer;
- (BOOL) validateUriTemplateVariableName: (NSString*) name;
@property (readonly) NSCharacterSet* disallowedChards;
@end

@implementation EZUriTemplateParser
{
    NSString* _uriTemplate;
    NSCharacterSet* _disallowedChars;
}

- (NSCharacterSet *) allowedChards{
    if(!self->_disallowedChars){
        NSMutableCharacterSet *allowed = [NSMutableCharacterSet alphanumericCharacterSet];
        [allowed formUnionWithCharacterSet:[NSCharacterSet characterSetWithCharactersInString:@"._%"]];
        
        self->_disallowedChars = [allowed invertedSet];
    }
    
    return self->_disallowedChards;
}

- (id)initWithTemplate:(NSString *)uriTemplate{
    if(self = [super init]){
        self->_uriTemplate = [uriTemplate copy];
        
        // default to standard factory;
        self.factory = [EZExpressionFactory new];
        return  self;
    }
    
    return nil;
}

- (NSArray *)expressions: (NSError **) error{
    
    if([self->_uriTemplate isEqualToString:@""]){
        return nil;
    }

    NSMutableArray* components = [[NSMutableArray alloc] init];
    
    NSUInteger l = [self->_uriTemplate length];
    
    NSMutableString *buffer;
    EZExpression *expr;
    
    for(int i = 0; i < l; i++){
        
        // where we at
        NSRange r = NSMakeRange(i, 1);
        
        // get the char we need;
        NSString *c = [self->_uriTemplate substringWithRange:r];
        
        // now decide what to do with it;
        
        
        // start of new expression
        if([c isEqualToString:@"{"]){
            
            if (self.factory.isInExpression) {
                // throw exception - cannot nest expressions
            }
            
            if(expr)
            {
                // finalize previous expression
                EZUriTemplateVariableSpec *var = [self getVariableFromString:buffer];
                [expr addVariableSpec:var];
            }
            
            
            // reset buffer
            buffer = nil;
            self.factory.inExpression = YES;
            continue;
        }
        
        if(!buffer){
            
            buffer = [c mutableCopy];
            expr = [self.factory expressionForExpansion:c];
            [components addObject:expr];
            
            // if its a simple expansion or a constant then keeo the first char
            if([expr isKindOfClass:[EZSimpleStringExpansionExpression class]] || [expr isKindOfClass:[EZConstantExpression class]])
                continue;
            
            
            // otherwise the first char represents a symbol denoting the
            // expansion type - so we want to clean it up
            buffer = [NSMutableString new];
            continue;
        }
    
        
        // end current variable and start of new
        // variable within expression
        if([c isEqualToString:@","]){
            if(expr)
            {
                // finalize previous expression
                EZUriTemplateVariableSpec *var = [self getVariableFromString:buffer];
                [expr addVariableSpec:var];
            }
            
            // now move to the next char
            r.location = ++i;
            buffer = [[self->_uriTemplate substringWithRange:r] mutableCopy];
            
            continue;
        }
        
        
        // end of expression
        if([c isEqualToString:@"}"]){
            if(!self.factory.isInExpression){
                // throw something
            }
            
            if(expr)
            {
                // finalize previous expression
                EZUriTemplateVariableSpec *var = [self getVariableFromString:buffer];
                [expr addVariableSpec:var];
            }
            
            // close out the expression
            self.factory.inExpression = false;
            buffer = nil;
            
            continue;
        }
        
        [buffer appendString:c];
    }
    
    if([buffer length]>0 && expr){
        // finalize previous expression
        EZUriTemplateVariableSpec *var = [self getVariableFromString:buffer];
        [expr addVariableSpec:var];
    }
    
    return components;
}

- (EZUriTemplateVariableSpec *)getVariableFromString:(NSString *)buffer{
    // check for modifiers
    EZUriTemplateVariableSpec *var = [[EZUriTemplateVariableSpec alloc] init];
    
    if([buffer hasSuffix:@"*"]){
        var.modifier = EZUriTemplateVariableModifierExploded;
        
        // hack off the last char to remove the modifier
        var.name = [[buffer substringToIndex:[buffer length] - 1] copy];
        
        return var;
    }

    
    NSError* err;
    
    __block EZUriTemplateVariableModifier mod = EZUriTemplateVariableModifierNone;
    __block uint prefixLength = 0;
    __block NSString* name = buffer;
    
    NSRegularExpression *regex = [NSRegularExpression
                                  regularExpressionWithPattern:PREFIX_MODIFIER_REGEX
                                                       options:NSRegularExpressionCaseInsensitive
                                                         error:&err];

    if(!buffer)
        return  var;
    
    [regex enumerateMatchesInString:buffer
                            options:NSMatchingWithoutAnchoringBounds
                              range:NSMakeRange(0, [buffer length])
                         usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
                             
                             // get the range of the number
                             NSRange matchRange = [result range];
                             NSRange numberRange = NSMakeRange(matchRange.location + 1, matchRange.length - 1);
                             
                             mod = EZUriTemplateVariablePrefixed;
                             prefixLength = [[buffer substringWithRange:numberRange] integerValue];
                             
                             name = [buffer substringToIndex:matchRange.location];
                         }];
    
    var.name = name;
    var.modifier = mod;
    var.prefixLength = prefixLength;
    
    
    return var;
}


- (BOOL)validateUriTemplateVariableName:(NSString *)name{
    return ([name rangeOfCharacterFromSet:self.disallowedChards].location == NSNotFound);
}
@end
