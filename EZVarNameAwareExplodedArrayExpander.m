//
//  EZVarNameAwareArrayExpander.m
//  uriTemplatesHost
//
//  Created by Donal Carpenter on 20/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import "EZVarNameAwareExplodedArrayExpander.h"

@implementation EZVarNameAwareExplodedArrayExpander

@synthesize delegate, successor;

+ (BOOL)canExpandType:(id)obj{
    return [obj isKindOfClass:[NSArray class]];
}

- (NSString *)expand:(id)obj forSpec:(EZUriTemplateVariableSpec *)spec{
    if(spec.modifier != EZUriTemplateVariableModifierExploded || ![[self class] canExpandType:obj])
    {
        self.successor.delegate = self.delegate;
        return [self.successor expand:obj forSpec:spec];
    }
    
    __block NSMutableString *uri = [NSMutableString new];
    __block NSString* separtor = @"";
    
    NSArray* array = (NSArray*) obj;
    
    EZVarNameAwareExplodedArrayExpander * __weak weakSelf = self;
    [array enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        if(![[weakSelf.delegate.characterMapExpander class] canExpandType:obj]){
            return;
        }
        
        NSString *value = [weakSelf.delegate.characterMapExpander expand:obj forSpec:spec];
        
        [uri appendFormat:@"%@%@", separtor, value];
        
        separtor = [weakSelf.delegate.expression separatorForSpec:spec];
    }];
    
    return uri;
}
@end
