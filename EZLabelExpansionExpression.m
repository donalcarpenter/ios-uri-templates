//
//  EZLabelExpansionExpression.m
//  uriTemplates
//
//  Created by Donal Carpenter on 10/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import "EZLabelExpansionExpression.h"

@implementation EZLabelExpansionExpression
- (NSString *)prependor{
    return @".";
}

- (NSString *)separatorForSpec:(EZUriTemplateVariableSpec *)variableSpec    {
    
    if(!variableSpec)
        return @".";
    
    return variableSpec.modifier == EZUriTemplateVariableModifierExploded ? @"." : @",";
}
@end
