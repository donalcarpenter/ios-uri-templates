//
//  EZPathSegmentExpansionTests.m
//  uriTemplatesHost
//
//  Created by Donal Carpenter on 16/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "EZUriTemplateParser.h"
#import "EZUriTemplateProcessor.h"
#import "EZExpressionFactory.h"
#import "EZConstantExpression.h"
#import "EZSimpleStringExpansionExpression.h"
#import "EZReservedStringExpansionExpression.h"
#import "EZFragmentExpansionExpression.h"
#import "EZLabelExpansionExpression.h"
#import "EZPathSegmentExpansionExpression.h"
#import "EZFormQueryExpansionExpression.h"
#import "EZFormContinuationExpansionExpression.h"
#import "EZPathStyleExpansionExpression.h"
#import "EZProhibitedIdentifierException.h"
#import "EZReservedIdentifierException.h"
#import "EZUriTemplateVariableSpec.h"
#import "EZOrderedDictionary.h"

@interface EZPathSegmentExpansionTests : XCTestCase

@end

@implementation EZPathSegmentExpansionTests
{
    NSDictionary *vars;
}

- (void)setUp
{
    [super setUp];
    
    
    self->vars = @{
                   @"count"  : @[@"one", @"two", @"three"],
                   @"dom"    : @[@"example", @"com"],
                   @"dub"    : @"me/too",
                   @"hello"  : @"Hello World!",
                   @"half"   : @"50%",
                   @"var"    : @"value",
                   @"who"    : @"fred",
                   @"base"   : @"http://example.com/home/",
                   @"path"   : @"/foo/bar",
                   @"list"   : @[@"red", @"green", @"blue"],
                   @"keys"   : [[EZOrderedDictionary alloc] initWithObjects:@[@";", @".", @","] forKeys:@[@"semi", @"dot", @"comma"]],
                   @"v"      : @"6",
                   @"x"      : @"1024",
                   @"y"      : @"768",
                   @"empty"  : @"",
                   @"empty_keys"   : @{},
                   @"undef"  : [NSNull null]
                   };
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}


- (EZExpression *) expression{
    return [EZPathSegmentExpansionExpression new];
}

- (void) testWhoVariable{
    EZExpression *s = [self expression];
    
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"who"]];
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:@[s] andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"/fred"], @"expanded to '%@' ", uri);
}

- (void) testWhoOnTheDouble{
    EZExpression *s = [self expression];
    
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"who"]];
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"who"]];
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:@[s] andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"/fred/fred"], @"expanded to '%@' ", uri);
}

- (void) testHalfWho{
    EZExpression *s = [self expression];
    
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"half"]];
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"who"]];
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:@[s] andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"/50%25/fred"], @"expanded to '%@' ", uri);
}

- (void) testWhoDub{
    EZExpression *s = [self expression];
    
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"who"]];
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"dub"]];
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:@[s] andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"/fred/me%2Ftoo"], @"expanded to '%@' ", uri);
}

- (void) testVariable{
    EZExpression *s = [self expression];
    
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"var"]];
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:@[s] andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"/value"], @"expanded to '%@' ", uri);
}

- (void) testEmpty{
    EZExpression *s = [self expression];
    
        [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"var"]];
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"empty"]];
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:@[s] andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"/value/"], @"half expanded to '%@' ", uri);
}

-(void) testUndef{
    EZExpression *s = [self expression];
    
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"var"]];
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"undef"]];
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:@[s] andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"/value"], @"half expanded to '%@' ", uri);
}


-(void) testWithPrefixModifier{
    EZExpression *s = [self expression];
    
    EZUriTemplateVariableSpec * varSpec = [[EZUriTemplateVariableSpec alloc] initWithName:@"var"];
    varSpec.modifier = EZUriTemplateVariablePrefixed;
    varSpec.prefixLength = 1;
    
    [s addVariableSpec:varSpec];
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"var"]];
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:@[s] andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"/v/value"], @"var expanded to '%@' ", uri);
}


-(void) testList{
    EZExpression *s = [self expression];
    
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"list"]];
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:@[s] andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"/red,green,blue"], @"x,empty expanded to '%@' ", uri);
}

-(void) testListExploded{
    EZExpression *s = [self expression];
    
    EZUriTemplateVariableSpec * varSpec = [[EZUriTemplateVariableSpec alloc] initWithName:@"list"];
    varSpec.modifier = EZUriTemplateVariableModifierExploded;
    
    [s addVariableSpec:varSpec];
    
    EZUriTemplateVariableSpec * varSpec2 = [[EZUriTemplateVariableSpec alloc] initWithName:@"path"];
    varSpec2.modifier = EZUriTemplateVariablePrefixed;
    varSpec2.prefixLength = 4;

        [s addVariableSpec:varSpec2];
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:@[s] andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"/red/green/blue/%2Ffoo"], @"x,empty expanded to '%@' ", uri);
}

-(void) testAssociativeArray{
    EZExpression *s = [self expression];
    
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"keys"]];
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:@[s] andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"/semi,%3B,dot,.,comma,%2C"], @"x,empty expanded to '%@' ", uri);
}

-(void) testAssociativeArrayExploded{
    EZExpression *s = [self expression];
    
    EZUriTemplateVariableSpec * varSpec = [[EZUriTemplateVariableSpec alloc] initWithName:@"keys"];
    varSpec.modifier = EZUriTemplateVariableModifierExploded;
    
    [s addVariableSpec:varSpec];
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:@[s] andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"/semi=%3B/dot=./comma=%2C"], @"x,empty expanded to '%@' ", uri);
}



@end
