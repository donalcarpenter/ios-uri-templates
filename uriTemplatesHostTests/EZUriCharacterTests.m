//
//  EZUriCharacterTests.m
//  uriTemplatesHost
//
//  Created by Donal Carpenter on 14/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "EZUriCharacter.h"

@interface EZUriCharacterTests : XCTestCase

@end

@implementation EZUriCharacterTests

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

- (void)testUnreserved
{
    EZUriCharacter *c = [[EZUriCharacter alloc] initWithString:@"a"];
    
    NSString *a = [c string];
    
    XCTAssert([a isEqualToString:@"a"], @"didn't get back the same output");
}

- (void)testReservedString
{
    EZUriCharacter *c = [[EZUriCharacter alloc] initWithString:@"!"];
    
    NSString *pct = [c string];
    NSString *res = [c reservedString];
    
    XCTAssert([res isEqualToString:@"!"], @"didn't get back the same output");
    
    XCTAssert([pct isEqualToString:@"%21"], @"didn't get back the same output");
}

- (void)testReservedCharCode
{
    EZUriCharacter *c = [[EZUriCharacter alloc] initWithCharCode:'!'];
    
    NSString *pct = [c string];
    NSString *res = [c reservedString];
    
    XCTAssert([res isEqualToString:@"!"], @"didn't get back the same output");
    
    XCTAssert([pct isEqualToString:@"%21"], @"didn't get back the same output");
}

- (void)testPCT
{
    EZUriCharacter *c = [[EZUriCharacter alloc] initWithPctEncodedString:@"%20"];
    
    NSString *pct = [c string];
    NSString *res = [c reservedString];
    
    XCTAssert([res isEqualToString:@"%20"], @"didn't get back the same output");
    
    XCTAssert([pct isEqualToString:@"%20"], @"didn't get back the same output");
}

@end
