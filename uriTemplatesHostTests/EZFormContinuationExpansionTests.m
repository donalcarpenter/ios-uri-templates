//
//  EZFormQueryExpansionTests.m
//  uriTemplatesHost
//
//  Created by Donal Carpenter on 16/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "EZUriTemplateParser.h"
#import "EZUriTemplateProcessor.h"
#import "EZExpressionFactory.h"
#import "EZConstantExpression.h"
#import "EZSimpleStringExpansionExpression.h"
#import "EZReservedStringExpansionExpression.h"
#import "EZFragmentExpansionExpression.h"
#import "EZLabelExpansionExpression.h"
#import "EZPathSegmentExpansionExpression.h"
#import "EZFormQueryExpansionExpression.h"
#import "EZFormContinuationExpansionExpression.h"
#import "EZPathStyleExpansionExpression.h"
#import "EZProhibitedIdentifierException.h"
#import "EZReservedIdentifierException.h"
#import "EZUriTemplateVariableSpec.h"
#import "EZOrderedDictionary.h"

@interface EZFormContinuationExpansionTests : XCTestCase

@end

@implementation EZFormContinuationExpansionTests
{
    NSDictionary *vars;
}

- (void)setUp
{
    [super setUp];
    
    
    self->vars = @{
                   @"count"  : @[@"one", @"two", @"three"],
                   @"dom"    : @[@"example", @"com"],
                   @"dub"    : @"me/too",
                   @"hello"  : @"Hello World!",
                   @"half"   : @"50%",
                   @"var"    : @"value",
                   @"who"    : @"fred",
                   @"base"   : @"http://example.com/home/",
                   @"path"   : @"/foo/bar",
                   @"list"   : @[@"red", @"green", @"blue"],
                   @"keys"   : [[EZOrderedDictionary alloc] initWithObjects:@[@";", @".", @","] forKeys:@[@"semi", @"dot", @"comma"]],
                   @"v"      : @"6",
                   @"x"      : @"1024",
                   @"y"      : @"768",
                   @"empty"  : @"",
                   @"empty_keys"   : @{},
                   @"undef"  : [NSNull null]
                   };
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

- (EZExpression *) expression{
    return [EZFormContinuationExpansionExpression new];
}

- (void) testWhoVariable{
    EZExpression *s = [self expression];
    
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"who"]];
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:@[s] andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"&who=fred"], @"expanded to '%@' ", uri);
}

- (void) testHalfWho{
    EZExpression *s = [self expression];
    
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"half"]];
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:@[s] andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"&half=50%25"], @"expanded to '%@' ", uri);
}

- (void) testMultiple{
    EZExpression *s = [self expression];
    
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"x"]];
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"y"]];
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:@[s] andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"&x=1024&y=768"], @"expanded to '%@' ", uri);
}

- (void) testEmptyInMultiple{
    EZExpression *s = [self expression];
    
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"x"]];
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"y"]];
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"empty"]];
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:@[s] andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"&x=1024&y=768&empty="], @"expanded to '%@' ", uri);
}

- (void) testGroupWithUndef{
    EZExpression *s = [self expression];
    
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"x"]];
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"y"]];
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"undef"]];
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:@[s] andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"&x=1024&y=768"], @"expanded to '%@' ", uri);
}

-(void) testWithPrefixModifier{
    EZExpression *s = [self expression];
    
    EZUriTemplateVariableSpec * varSpec = [[EZUriTemplateVariableSpec alloc] initWithName:@"var"];
    varSpec.modifier = EZUriTemplateVariablePrefixed;
    varSpec.prefixLength = 3;
    
    [s addVariableSpec:varSpec];
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:@[s] andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"&var=val"], @"var expanded to '%@' ", uri);
}

-(void) testList{
    EZExpression *s = [self expression];
    
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"list"]];
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:@[s] andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"&list=red,green,blue"], @"expanded to '%@' ", uri);
}

-(void) testListExploded{
    EZExpression *s = [self expression];
    
    EZUriTemplateVariableSpec * varSpec = [[EZUriTemplateVariableSpec alloc] initWithName:@"list"];
    varSpec.modifier = EZUriTemplateVariableModifierExploded;
    
    [s addVariableSpec:varSpec];
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:@[s] andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"&list=red&list=green&list=blue"], @"x,empty expanded to '%@' ", uri);
}

-(void) testAssociativeArray{
    EZExpression *s = [self expression];
    
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"keys"]];
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:@[s] andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"&keys=semi,%3B,dot,.,comma,%2C"], @"x,empty expanded to '%@' ", uri);
}

-(void) testAssociativeArrayExploded{
    EZExpression *s = [self expression];
    
    EZUriTemplateVariableSpec * varSpec = [[EZUriTemplateVariableSpec alloc] initWithName:@"keys"];
    varSpec.modifier = EZUriTemplateVariableModifierExploded;
    
    [s addVariableSpec:varSpec];
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:@[s] andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"&semi=%3B&dot=.&comma=%2C"], @"x,empty expanded to '%@' ", uri);
}


@end
