//
//  EZExpansionTests.m
//  uriTemplatesHost
//
//  Created by Donal Carpenter on 15/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "EZUriTemplateParser.h"
#import "EZUriTemplateProcessor.h"
#import "EZExpressionFactory.h"
#import "EZConstantExpression.h"
#import "EZSimpleStringExpansionExpression.h"
#import "EZReservedStringExpansionExpression.h"
#import "EZFragmentExpansionExpression.h"
#import "EZLabelExpansionExpression.h"
#import "EZPathSegmentExpansionExpression.h"
#import "EZFormQueryExpansionExpression.h"
#import "EZFormContinuationExpansionExpression.h"
#import "EZPathStyleExpansionExpression.h"
#import "EZProhibitedIdentifierException.h"
#import "EZReservedIdentifierException.h"
#import "EZUriTemplateVariableSpec.h"
#import "EZOrderedDictionary.h"

@interface EZExpansionTests : XCTestCase

@end

@implementation EZExpansionTests
{
    NSDictionary *vars;
}
- (void)setUp
{
    [super setUp];

    self->vars = @{
                  @"count"  : @[@"one", @"two", @"three"],
                  @"dom"    : @[@"example", @"com"],
                  @"dub"    : @"me/too",
                  @"hello"  : @"Hello World!",
                  @"half"   : @"50%",
                  @"var"    : @"value",
                  @"who"    : @"fred",
                  @"base"   : @"http://example.com/home/",
                  @"path"   : @"/foo/bar",
                  @"list"   : @[@"red", @"green", @"blue"],
                  @"keys"   : [[EZOrderedDictionary alloc] initWithObjects:@[@";", @".", @","] forKeys:@[@"semi", @"dot", @"comma"]],
                  @"v"      : @"6",
                  @"y"      : @"1024",
                  @"z"      : @"768",
                  @"empty"  : @"",
                  @"empty_keys"   : @{},
                  @"undef"  : [NSNull null]
                  };
    
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

- (void)testSimpleExpansion
{

    EZExpressionFactory *factory = [EZExpressionFactory new];
    factory.inExpression = YES;
    EZExpression *s = [factory expressionForExpansion:@"a"];
    


    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"count"]];

    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:@[s] andVariables:vars];

    NSString *uri = [s expand:p.variables];

    XCTAssert([uri isEqualToString:@"one,two,three"], @"count expanded to '%@' ", uri);
    
}


- (void)testSimpleMultipleExpansion
{
    
    EZExpressionFactory *factory = [EZExpressionFactory new];
    factory.inExpression = YES;
    EZExpression *s = [factory expressionForExpansion:@"a"];
    
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"count"]];
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"var"]];
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"hello"]];
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"undef"]];
    
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:nil andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"one,two,three,value,Hello%20World%21"], @"multiple expanded to '%@' ", uri);
    
}

- (void)testSimpleAnotherMultipleExpansion
{
    
    EZExpressionFactory *factory = [EZExpressionFactory new];
    factory.inExpression = YES;
    EZExpression *s = [factory expressionForExpansion:@"a"];
    
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"half"]];

    EZUriTemplateVariableSpec *spec = [[EZUriTemplateVariableSpec alloc] initWithName:@"var"];
    spec.modifier = EZUriTemplateVariablePrefixed;
    spec.prefixLength = 3;
    [s addVariableSpec:spec];
    
    
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:nil andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"50%25,val"], @"another multiple expanded to '%@' ", uri);
}


- (void)testSimpleExplodedDictionaryExpansion
{
    
    EZExpressionFactory *factory = [EZExpressionFactory new];
    factory.inExpression = YES;
    EZExpression *s = [factory expressionForExpansion:@"a"];
    
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"keys"]];
    
    EZUriTemplateVariableSpec *spec = [[EZUriTemplateVariableSpec alloc] initWithName:@"keys"];
    spec.modifier = EZUriTemplateVariableModifierExploded;
    [s addVariableSpec:spec];
    
    
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:nil andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"semi,%3B,dot,.,comma,%2C,semi=%3B,dot=.,comma=%2C"], @"simple exploded expanded to '%@' ", uri);
}

@end
