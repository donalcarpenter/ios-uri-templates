//
//  EZUriParserTests.m
//  uriTemplatesHost
//
//  Created by Donal Carpenter on 13/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "EZUriTemplateParser.h"

#import "EZExpressionFactory.h"
#import "EZConstantExpression.h"
#import "EZSimpleStringExpansionExpression.h"
#import "EZReservedStringExpansionExpression.h"
#import "EZFragmentExpansionExpression.h"
#import "EZLabelExpansionExpression.h"
#import "EZPathSegmentExpansionExpression.h"
#import "EZFormQueryExpansionExpression.h"
#import "EZFormContinuationExpansionExpression.h"
#import "EZPathStyleExpansionExpression.h"
#import "EZProhibitedIdentifierException.h"
#import "EZReservedIdentifierException.h"
#import "EZUriTemplateVariableSpec.h"

@interface EZUriParserTests : XCTestCase

@end

@implementation EZUriParserTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testFor1ConstantConstant;
{
    
    NSString *c = @"/nothinghere";
    
    
    EZUriTemplateParser *p = [[EZUriTemplateParser alloc] initWithTemplate:c];
    NSError *e;
    
    NSArray *components = [p expressions:&e];
    
    unsigned long count = [components count];
    unsigned long expected = 1lu;

    XCTAssertEqual(expected, count, @"Expected 1 item in components. Found: %lu", count);
    
    
    XCTAssertTrue([components[0] isKindOfClass:[EZConstantExpression class]], @"constant did not return correct type, returned %@", [components[0] class]);
}

- (void)testRegex{
    #define PREFIX_MODIFIER_REGEX @":([1-9]{1,1})(\\d{0,4})$"
    
    NSError* err;

    
    NSRegularExpression *regex = [NSRegularExpression
                                  regularExpressionWithPattern:PREFIX_MODIFIER_REGEX
                                  options:NSRegularExpressionCaseInsensitive
                                  error:&err];
    
    NSString *str1 = @"blah:123";
    __block BOOL found1 = NO;
    [regex enumerateMatchesInString:str1 options:NSMatchingWithTransparentBounds range:NSMakeRange(0, [str1 length]) usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
        XCTAssert(4 == result.range.location, @"Expected location in %@ ws 4, actual %d", str1, result.range.location);
        found1 = YES;
    }];
    XCTAssertTrue(found1, @"str1 not found");

    NSString *str2 = @"blah:7";
    __block BOOL found2 = NO;
    [regex enumerateMatchesInString:str2 options:NSMatchingWithTransparentBounds range:NSMakeRange(0, [str2 length]) usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
        XCTAssert(4 == result.range.location, @"Expected location in %@ ws 4, actual %d", str2, result.range.location);
        found2 = YES;
    }];
    XCTAssertTrue(found2, @"str2 not found");
    
    NSString *str3 = @"blah:012";
    __block BOOL found3 = NO;
    [regex enumerateMatchesInString:str3 options:NSMatchingWithTransparentBounds range:NSMakeRange(0, [str3 length]) usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
        found3 = YES;
    }];
    XCTAssertFalse(found3, @"str3 was found");
    
    NSString *str4 = @"blah:999999";
    __block BOOL found4 = NO;
    [regex enumerateMatchesInString:str4 options:NSMatchingWithTransparentBounds range:NSMakeRange(0, [str4 length]) usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
        found4 = YES;
    }];
    XCTAssertFalse(found4, @"str3 was found");
}

- (void)testWithSimpleSingleExpression{
    NSString *c = @"/nothinghere{somethingHere}";
    
    
    EZUriTemplateParser *p = [[EZUriTemplateParser alloc] initWithTemplate:c];
    NSError *e;
    
    NSArray *components = [p expressions:&e];
    
    unsigned long count = [components count];
    unsigned long expected = 2lu;
    
    XCTAssertEqual(expected, count, @"Expected 2 item in components. Found: %lu", count);
    
    
    XCTAssertTrue([components[0] isKindOfClass:[EZConstantExpression class]], @"constant did not return correct type, returned %@", [components[0] class]);
    
    XCTAssertTrue([components[1] isKindOfClass:[EZSimpleStringExpansionExpression class]], @"simple expression did not return correct type, returned %@", [components[0] class]);
    
    
    EZSimpleStringExpansionExpression *expr = components[1];
    XCTAssert(1 == [expr.variableSpecs count], "incorrect number of variables assigned to expression");
    
    EZUriTemplateVariableSpec *var = expr.variableSpecs[0];
    
    XCTAssert([@"somethingHere" isEqualToString:var.name], @"Variable identity should be 'somethingHere' but is %@", var.name);
    XCTAssert(EZUriTemplateVariableModifierNone == var.modifier, "bad modifier %d", var.modifier);

}

- (void)testWithSimpleMultipleExpression{
    NSString *c = @"/nothinghere{somethingHere,andHere,yupHereToo}";
    
    
    EZUriTemplateParser *p = [[EZUriTemplateParser alloc] initWithTemplate:c];
    NSError *e;
    
    NSArray *components = [p expressions:&e];
    
    unsigned long count = [components count];
    unsigned long expected = 2lu;
    
    XCTAssertEqual(expected, count, @"Expected 2 item in components. Found: %lu", count);
    
    
    XCTAssertTrue([components[0] isKindOfClass:[EZConstantExpression class]], @"constant did not return correct type, returned %@", [components[0] class]);
    
    XCTAssertTrue([components[1] isKindOfClass:[EZSimpleStringExpansionExpression class]], @"simple expression did not return correct type, returned %@", [components[0] class]);
    
    
    EZSimpleStringExpansionExpression *expr = components[1];
    XCTAssert(3 == [expr.variableSpecs count], "incorrect number of variables assigned to expression");
    
    EZUriTemplateVariableSpec *var1 = expr.variableSpecs[0];
    EZUriTemplateVariableSpec *var2 = expr.variableSpecs[1];
    EZUriTemplateVariableSpec *var3 = expr.variableSpecs[2];
    
    XCTAssert([@"somethingHere" isEqualToString:var1.name], @"Variable identity should be 'somethingHere' but is %@", var1.name);
    XCTAssert([@"andHere" isEqualToString:var2.name], @"Variable identity should be 'andHere' but is %@", var2.name);
    XCTAssert([@"yupHereToo" isEqualToString:var3.name], @"Variable identity should be 'yupHereToo' but is %@", var3.name);
}


- (void)testWithSimpleSingleExpressionWithExplode{
    NSString *c = @"/nothinghere{somethingHere*}";
    
    
    EZUriTemplateParser *p = [[EZUriTemplateParser alloc] initWithTemplate:c];
    NSError *e;
    
    NSArray *components = [p expressions:&e];
    
    unsigned long count = [components count];
    unsigned long expected = 2lu;
    
    XCTAssertEqual(expected, count, @"Expected 2 item in components. Found: %lu", count);
    
    
    XCTAssertTrue([components[0] isKindOfClass:[EZConstantExpression class]], @"constant did not return correct type, returned %@", [components[0] class]);
    
    XCTAssertTrue([components[1] isKindOfClass:[EZSimpleStringExpansionExpression class]], @"simple expression did not return correct type, returned %@", [components[0] class]);
    
    
    EZSimpleStringExpansionExpression *expr = components[1];
    XCTAssert(1 == [expr.variableSpecs count], "incorrect number of variables assigned to expression");
    
    EZUriTemplateVariableSpec *var = expr.variableSpecs[0];
    
    XCTAssert([@"somethingHere" isEqualToString:var.name], @"Variable identity should be 'somethingHere' but is %@", var.name);
    XCTAssert(EZUriTemplateVariableModifierExploded == var.modifier, "Modifier was not exploded");
}


- (void)testWithSimpleSingleExpressionWithValidPrefix{
    NSString *c = @"/nothinghere{somethingHere:12}";
    
    
    EZUriTemplateParser *p = [[EZUriTemplateParser alloc] initWithTemplate:c];
    NSError *e;
    
    NSArray *components = [p expressions:&e];
    
    unsigned long count = [components count];
    unsigned long expected = 2lu;
    
    XCTAssertEqual(expected, count, @"Expected 2 item in components. Found: %lu", count);
    
    
    XCTAssertTrue([components[0] isKindOfClass:[EZConstantExpression class]], @"constant did not return correct type, returned %@", [components[0] class]);
    
    XCTAssertTrue([components[1] isKindOfClass:[EZSimpleStringExpansionExpression class]], @"simple expression did not return correct type, returned %@", [components[0] class]);
    
    
    EZSimpleStringExpansionExpression *expr = components[1];
    XCTAssert(1 == [expr.variableSpecs count], "incorrect number of variables assigned to expression");
    
    EZUriTemplateVariableSpec *var = expr.variableSpecs[0];
    
    XCTAssert([@"somethingHere" isEqualToString:var.name], @"Variable identity should be 'somethingHere' but is %@", var.name);
    XCTAssert(EZUriTemplateVariablePrefixed == var.modifier, "Modifier was not prefixed");
    XCTAssert(12 == var.prefixLength, "Prefix length should have been 12 but was %d", var.prefixLength);
}

- (void)testWithSimpleMultipleExpressionWithModifiers{
    NSString *c = @"/nothinghere{somethingHere*,andHere:2,yupHereToo:999}";
    
    
    EZUriTemplateParser *p = [[EZUriTemplateParser alloc] initWithTemplate:c];
    NSError *e;
    
    NSArray *components = [p expressions:&e];
    
    unsigned long count = [components count];
    unsigned long expected = 2lu;
    
    XCTAssertEqual(expected, count, @"Expected 2 item in components. Found: %lu", count);
    
    
    XCTAssertTrue([components[0] isKindOfClass:[EZConstantExpression class]], @"constant did not return correct type, returned %@", [components[0] class]);
    
    XCTAssertTrue([components[1] isKindOfClass:[EZSimpleStringExpansionExpression class]], @"simple expression did not return correct type, returned %@", [components[0] class]);
    
    
    EZSimpleStringExpansionExpression *expr = components[1];
    XCTAssert(3 == [expr.variableSpecs count], "incorrect number of variables assigned to expression");
    
    EZUriTemplateVariableSpec *var1 = expr.variableSpecs[0];
    EZUriTemplateVariableSpec *var2 = expr.variableSpecs[1];
    EZUriTemplateVariableSpec *var3 = expr.variableSpecs[2];
    
    XCTAssert([@"somethingHere" isEqualToString:var1.name], @"Variable identity should be 'somethingHere' but is %@", var1.name);
    XCTAssert(EZUriTemplateVariableModifierExploded == var1.modifier, @"var1 modifier should be exploded but is %u", var1.modifier);
    
    XCTAssert([@"andHere" isEqualToString:var2.name], @"Variable identity should be 'andHere' but is %@", var2.name);
    XCTAssert(EZUriTemplateVariablePrefixed == var2.modifier, @"var2 modifier should be prefixed but it is %u", var2.modifier);
    XCTAssert(2 == var2.prefixLength, @"var2 prefix length should be 2 but it %d", var2.prefixLength);
    
    XCTAssert([@"yupHereToo" isEqualToString:var3.name], @"Variable identity should be 'yupHereToo' but is %@", var3.name);
    XCTAssert(EZUriTemplateVariablePrefixed == var3.modifier, @"var3 modifier should be prefixed but it is %u", var3.modifier);
    XCTAssert(999 == var3.prefixLength, @"var3 prefix length should be 999 but it %d", var3.prefixLength);
    
}

@end
