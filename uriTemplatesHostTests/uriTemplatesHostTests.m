//
//  uriTemplatesHostTests.m
//  uriTemplatesHostTests
//
//  Created by Donal Carpenter on 10/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "EZExpression.h"
#import "EZExpressionFactory.h"
#import "EZSimpleStringExpansionExpression.h"
#import "EZReservedStringExpansionExpression.h"
#import "EZFragmentExpansionExpression.h"
#import "EZLabelExpansionExpression.h"
#import "EZPathSegmentExpansionExpression.h"
#import "EZFormQueryExpansionExpression.h"
#import "EZFormContinuationExpansionExpression.h"
#import "EZConstantExpression.h"
#import "EZPathStyleExpansionExpression.h"

@interface uriTemplatesHostTests : XCTestCase

@end

@implementation uriTemplatesHostTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testReturnsCorrectExpansionInstanceTypes
{

    EZExpressionFactory *factory = [EZExpressionFactory new];
    
    factory.inExpression = YES;

    EZExpression* expr1 = [factory expressionForExpansion:@"+"];
    XCTAssertTrue([expr1 isKindOfClass:[EZReservedStringExpansionExpression class]], @"+ did not return correct type");
    
    EZExpression* expr2 = [factory expressionForExpansion:@"#"];
    XCTAssertTrue([expr2 isKindOfClass:[EZFragmentExpansionExpression class]], @"@ did not return correct type");
    
    EZExpression* expr3 = [factory expressionForExpansion:@"."];
    XCTAssertTrue([expr3 isKindOfClass:[EZLabelExpansionExpression class]], @". did not return correct type");
    
    EZExpression* expr4 = [factory expressionForExpansion:@"/"];
    XCTAssertTrue([expr4 isKindOfClass:[EZPathSegmentExpansionExpression class]], @"/ did not return correct type");
    
    EZExpression* expr5 = [factory expressionForExpansion:@"?"];
    XCTAssertTrue([expr5 isKindOfClass:[EZFormQueryExpansionExpression class]], @"? did not return correct type");
    
    EZExpression* expr6 = [factory expressionForExpansion:@"&"];
    XCTAssertTrue([expr6 isKindOfClass:[EZFormContinuationExpansionExpression class]], @"& did not return correct type");
    
    EZExpression* expr7 = [factory expressionForExpansion:@";"];
    XCTAssertTrue([expr7 isKindOfClass:[EZPathStyleExpansionExpression class]], @"; did not return correct type");
    
    EZExpression* expr9 = [factory expressionForExpansion:@"a"];
    XCTAssertTrue([expr9 isKindOfClass:[EZSimpleStringExpansionExpression class]], @"simple expression did not return correct type");

    factory.inExpression = NO;
    
    EZExpression* expr8 = [factory expressionForExpansion:@"a"];
    XCTAssertTrue([expr8 isKindOfClass:[EZConstantExpression class]], @"constant did not return correct type");
}
@end
