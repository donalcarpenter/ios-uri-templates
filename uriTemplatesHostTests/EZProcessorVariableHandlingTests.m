//
//  EZProcessorVariableHandlingTests.m
//  uriTemplatesHost
//
//  Created by Donal Carpenter on 15/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "EZUriTemplateProcessor.h"

@interface EZProcessorVariableHandlingTests : XCTestCase

@end

@implementation EZProcessorVariableHandlingTests

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

- (void)testExample
{

    // take from http://tools.ietf.org/html/rfc6570#section-3.2
    
    NSDictionary* vars =
    @{
          @"count"  : @[@"one", @"two", @"three"],
          @"dom"    : @[@"example", @"com"],
          @"dub"    : @"me/too",
          @"hello"  : @"Hello World!",
          @"half"   : @"50%",
          @"var"    : @"value",
          @"who"    : @"fred",
          @"base"   : @"http://example.com/home/",
          @"path"   : @"/foo/bar",
          @"list"   : @[@"red", @"green", @"blue"],
          @"keys"   : @{@"semi": @";", @"dot": @".", @"comma": @","},
          @"v"      : @"6",
          @"y"      : @"1024",
          @"z"      : @"768",
          @"empty"  : @"",
          @"empty_keys"   : @{},
          @"undef"  : [NSNull null]
      };
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:nil andVariables:vars];
    
    NSDictionary* d = p.variables;
    
    XCTAssert([d count] == ([vars count] - 1), @"Counts don't match. vars has %d and d has %d", [vars count], [d count]);
    
    XCTAssert(d, @"variables are no returned");
    
    XCTAssert([[d[@"var"
                  ] string] isEqualToString:@"value"], @"var");
    XCTAssert([[d[@"hello"] string] isEqualToString:@"Hello%20World%21"], @"hello");
    XCTAssert([[d[@"half"] string] isEqualToString:@"50%25"], @"half");
    XCTAssert([[d[@"path"] string] isEqualToString:@"%2Ffoo%2Fbar"], @"path");
    XCTAssert([[d[@"path"] reservedString] isEqualToString:@"/foo/bar"], @"path reserved");
    
    XCTAssert(3 == [d[@"list"] count], @"count of list");
    XCTAssert(3 == [d[@"keys"] count], @"count of list");
    
    XCTAssert(0 == [d[@"empty_keys"] count], @"count of list");
}

@end
