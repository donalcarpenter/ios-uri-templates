//
//  EZSimpleExpansionTests.m
//  uriTemplatesHost
//
//  Created by Donal Carpenter on 16/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "EZUriTemplateParser.h"
#import "EZUriTemplateProcessor.h"
#import "EZExpressionFactory.h"
#import "EZConstantExpression.h"
#import "EZSimpleStringExpansionExpression.h"
#import "EZReservedStringExpansionExpression.h"
#import "EZFragmentExpansionExpression.h"
#import "EZLabelExpansionExpression.h"
#import "EZPathSegmentExpansionExpression.h"
#import "EZFormQueryExpansionExpression.h"
#import "EZFormContinuationExpansionExpression.h"
#import "EZPathStyleExpansionExpression.h"
#import "EZProhibitedIdentifierException.h"
#import "EZReservedIdentifierException.h"
#import "EZUriTemplateVariableSpec.h"
#import "EZOrderedDictionary.h"

@interface EZSimpleExpansionTests : XCTestCase

@end

@implementation EZSimpleExpansionTests
{
    NSDictionary *vars;
}

- (void)setUp
{
    [super setUp];

    
    self->vars = @{
                   @"count"  : @[@"one", @"two", @"three"],
                   @"dom"    : @[@"example", @"com"],
                   @"dub"    : @"me/too",
                   @"hello"  : @"Hello World!",
                   @"half"   : @"50%",
                   @"var"    : @"value",
                   @"who"    : @"fred",
                   @"base"   : @"http://example.com/home/",
                   @"path"   : @"/foo/bar",
                   @"list"   : @[@"red", @"green", @"blue"],
                   @"keys"   : [[EZOrderedDictionary alloc] initWithObjects:@[@";", @".", @","] forKeys:@[@"semi", @"dot", @"comma"]],
                   @"v"      : @"6",
                   @"x"      : @"1024",
                   @"y"      : @"768",
                   @"empty"  : @"",
                   @"empty_keys"   : @{},
                   @"undef"  : [NSNull null]
                   };
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}


- (EZExpression *) expression{
    return [EZSimpleStringExpansionExpression new];
}


- (void) testVar{
    EZExpression *s = [self expression];
    
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"var"]];
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:@[s] andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"value"], @"var expanded to '%@' ", uri);
}

- (void) testHelloWorld{
    EZExpression *s = [self expression];
    
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"hello"]];
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:@[s] andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"Hello%20World%21"], @"hello expanded to '%@' ", uri);
}

-(void) testHalf{
    EZExpression *s = [self expression];
    
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"half"]];
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:@[s] andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"50%25"], @"half expanded to '%@' ", uri);
}

- (void) testEmpty{
    EZExpression *s = [self expression];
    
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"x"]];
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"empty"]];
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:@[s] andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"1024,"], @"x,empty expanded to '%@' ", uri);
}

- (void) testAppendingUndef{
    EZExpression *s = [self expression];
    
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"x"]];
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"undef"]];
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:@[s] andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"1024"], @"x,empty expanded to '%@' ", uri);
}

- (void) testPrependingUndef{
    EZExpression *s = [self expression];
    
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"undef"]];
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"y"]];
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:@[s] andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"768"], @"x,empty expanded to '%@' ", uri);
}

- (void) testMultipleVariables{
    EZExpression *s = [self expression];
    
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"x"]];
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"hello"]];
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"y"]];
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:@[s] andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"1024,Hello%20World%21,768"], @"x,empty expanded to '%@' ", uri);
}

-(void) testWithPrefixModifier{
    EZExpression *s = [self expression];
    
    EZUriTemplateVariableSpec * varSpec = [[EZUriTemplateVariableSpec alloc] initWithName:@"var"];
    varSpec.modifier = EZUriTemplateVariablePrefixed;
    varSpec.prefixLength = 3;
    
    [s addVariableSpec:varSpec];
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:@[s] andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"val"], @"var expanded to '%@' ", uri);
}

-(void) testWithTooLongPrefixModifier{
    EZExpression *s = [self expression];
    
    EZUriTemplateVariableSpec * varSpec = [[EZUriTemplateVariableSpec alloc] initWithName:@"var"];
    varSpec.modifier = EZUriTemplateVariablePrefixed;
    varSpec.prefixLength = 30;
    
    [s addVariableSpec:varSpec];
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:@[s] andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"value"], @"var expanded to '%@' ", uri);
}


-(void) testList{
    EZExpression *s = [self expression];
    
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"list"]];
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:@[s] andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"red,green,blue"], @"x,empty expanded to '%@' ", uri);
}

-(void) testListExploded{
    EZExpression *s = [self expression];
    
    EZUriTemplateVariableSpec * varSpec = [[EZUriTemplateVariableSpec alloc] initWithName:@"list"];
    varSpec.modifier = EZUriTemplateVariableModifierExploded;
    
    [s addVariableSpec:varSpec];
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:@[s] andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"red,green,blue"], @"x,empty expanded to '%@' ", uri);
}

-(void) testAssociativeArray{
    EZExpression *s = [self expression];
    
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"keys"]];
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:@[s] andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"semi,%3B,dot,.,comma,%2C"], @"x,empty expanded to '%@' ", uri);
}

-(void) testAssociativeArrayExploded{
    EZExpression *s = [self expression];
    
    EZUriTemplateVariableSpec * varSpec = [[EZUriTemplateVariableSpec alloc] initWithName:@"keys"];
    varSpec.modifier = EZUriTemplateVariableModifierExploded;
    
    [s addVariableSpec:varSpec];
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:@[s] andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"semi=%3B,dot=.,comma=%2C"], @"x,empty expanded to '%@' ", uri);
}

- (void) testUrl{
    EZExpression *s = [self expression];
    
    [s addVariableSpec:[[EZUriTemplateVariableSpec alloc] initWithName:@"base"]];
    
    EZUriTemplateProcessor *p = [[EZUriTemplateProcessor alloc] initWithExpressions:@[s] andVariables:vars];
    
    NSString *uri = [s expand:p.variables];
    
    XCTAssert([uri isEqualToString:@"http%3A%2F%2Fexample.com%2Fhome%2F"], @"var expanded to '%@' ", uri);
}

@end
