//
//  EZLabelExpansionParseTests.m
//  uriTemplatesHost
//
//  Created by Donal Carpenter on 14/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "EZUriTemplateParser.h"

#import "EZExpressionFactory.h"
#import "EZConstantExpression.h"
#import "EZSimpleStringExpansionExpression.h"
#import "EZReservedStringExpansionExpression.h"
#import "EZFragmentExpansionExpression.h"
#import "EZLabelExpansionExpression.h"
#import "EZPathSegmentExpansionExpression.h"
#import "EZFormQueryExpansionExpression.h"
#import "EZFormContinuationExpansionExpression.h"
#import "EZPathStyleExpansionExpression.h"
#import "EZProhibitedIdentifierException.h"
#import "EZReservedIdentifierException.h"
#import "EZUriTemplateVariableSpec.h"

@interface EZLabelExpansionParseTests : XCTestCase

@end

@implementation EZLabelExpansionParseTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}



- (void)testWithLabelSingleExpression{
    NSString *c = @"/nothinghere{.somethingHere}";
    
    
    EZUriTemplateParser *p = [[EZUriTemplateParser alloc] initWithTemplate:c];
    NSError *e;
    
    NSArray *components = [p expressions:&e];
    
    unsigned long count = [components count];
    unsigned long expected = 2lu;
    
    XCTAssertEqual(expected, count, @"Expected 2 item in components. Found: %lu", count);
    
    
    XCTAssertTrue([components[0] isKindOfClass:[EZConstantExpression class]], @"constant did not return correct type, returned %@", [components[0] class]);
    
    XCTAssertTrue([components[1] isKindOfClass:[EZLabelExpansionExpression class]], @"Label expression did not return correct type, returned %@", [components[0] class]);
    
    
    EZLabelExpansionExpression *expr = components[1];
    XCTAssert(1 == [expr.variableSpecs count], "incorrect number of variables assigned to expression");
    
    EZUriTemplateVariableSpec *var = expr.variableSpecs[0];
    
    XCTAssert([@"somethingHere" isEqualToString:var.name], @"Variable identity should be 'somethingHere' but is %@", var.name);
    XCTAssert(EZUriTemplateVariableModifierNone == var.modifier, "bad modifier %d", var.modifier);
    
}


- (void)testWithLabelMultipleExpression{
    NSString *c = @"/nothinghere{.somethingHere,andHere,yupHereToo}";
    
    
    EZUriTemplateParser *p = [[EZUriTemplateParser alloc] initWithTemplate:c];
    NSError *e;
    
    NSArray *components = [p expressions:&e];
    
    unsigned long count = [components count];
    unsigned long expected = 2lu;
    
    XCTAssertEqual(expected, count, @"Expected 2 item in components. Found: %lu", count);
    
    
    XCTAssertTrue([components[0] isKindOfClass:[EZConstantExpression class]], @"constant did not return correct type, returned %@", [components[0] class]);
    
    XCTAssertTrue([components[1] isKindOfClass:[EZLabelExpansionExpression class]], @"label expression did not return correct type, returned %@", [components[0] class]);
    
    
    EZFragmentExpansionExpression *expr = components[1];
    XCTAssert(3 == [expr.variableSpecs count], "incorrect number of variables assigned to expression");
    
    EZUriTemplateVariableSpec *var1 = expr.variableSpecs[0];
    EZUriTemplateVariableSpec *var2 = expr.variableSpecs[1];
    EZUriTemplateVariableSpec *var3 = expr.variableSpecs[2];
    
    XCTAssert([@"somethingHere" isEqualToString:var1.name], @"Variable identity should be 'somethingHere' but is %@", var1.name);
    XCTAssert([@"andHere" isEqualToString:var2.name], @"Variable identity should be 'andHere' but is %@", var2.name);
    XCTAssert([@"yupHereToo" isEqualToString:var3.name], @"Variable identity should be 'yupHereToo' but is %@", var3.name);
}


@end
