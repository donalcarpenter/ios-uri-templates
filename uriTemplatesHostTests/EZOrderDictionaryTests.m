//
//  EZOrderDictionaryTests.m
//  uriTemplatesHost
//
//  Created by Donal Carpenter on 16/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "EZOrderedDictionary.h"

@interface EZOrderDictionaryTests : XCTestCase

@end

@implementation EZOrderDictionaryTests

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

- (void)testOrderedDictionary
{

    EZOrderedDictionary *dict = [[EZOrderedDictionary alloc] init];
    
    [dict setObject:@"a" forKey:@1];
    [dict setObject:@"b" forKey:@2];
    [dict setObject:@"c" forKey:@3];
    
    
    __block int i = 0;
    
    [dict enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        
        NSNumber *num = (NSNumber*)key;
        
        XCTAssert([num isEqualToNumber:[NSNumber numberWithInt:++i]], @"number ain't right");
    }];
    
    XCTAssert([[dict objectAtIndex:0] isEqualToString:@"a"], @"meh - index didn't work out");
    XCTAssert([[dict objectAtIndex:1] isEqualToString:@"b"], @"meh - index didn't work out");
    XCTAssert([[dict objectAtIndex:2] isEqualToString:@"c"], @"meh - index didn't work out");
}

- (void)testOrderedDictionaryWithInitializedValues
{
    
    EZOrderedDictionary *dict = [[EZOrderedDictionary alloc] initWithObjects:@[@"a", @"b", @"c"] forKeys:@[@1,@2,@3]];
    __block int i = 0;
    
    [dict enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        
        NSNumber *num = (NSNumber*)key;
        
        XCTAssert([num isEqualToNumber:[NSNumber numberWithInt:++i]], @"number ain't right");
    }];
    
    XCTAssert([[dict objectAtIndex:0] isEqualToString:@"a"], @"meh - index didn't work out");
    XCTAssert([[dict objectAtIndex:1] isEqualToString:@"b"], @"meh - index didn't work out");
    XCTAssert([[dict objectAtIndex:2] isEqualToString:@"c"], @"meh - index didn't work out");
}

@end
