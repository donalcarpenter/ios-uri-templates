//
//  EZUrlParsingTests.m
//  uriTemplatesHost
//
//  Created by Donal Carpenter on 14/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "EZExpression.h"

@interface EZUrlParsingTests : XCTestCase

@end

@implementation EZUrlParsingTests

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}


- (NSString *) urlify: (NSString*) original reserved: (BOOL) reserved{
    
    CFStringRef reservedChars = (__bridge CFStringRef)(@":/?#[]@!$&'()*+,;=");
    
    CFStringRef leaveUnescaped = reserved ? reservedChars : NULL;
    CFStringRef encodeAnyway = reserved ? NULL :reservedChars ;
    
    
    CFStringRef removePctEncoding = CFURLCreateStringByReplacingPercentEscapes(kCFAllocatorDefault, (__bridge CFStringRef)(original), CFSTR(""));
    
    NSString *url = CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, removePctEncoding, leaveUnescaped, encodeAnyway, kCFStringEncodingUTF8));
    
    return url;
    
}

@end
