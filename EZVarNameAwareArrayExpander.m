//
//  EZVarNameAwareArrayExpander.m
//  uriTemplatesHost
//
//  Created by Donal Carpenter on 20/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import "EZVarNameAwareArrayExpander.h"
#import "EZUriCharacterMap.h"

@implementation EZVarNameAwareArrayExpander
@synthesize delegate, successor;

+ (BOOL)canExpandType:(id)obj{
    return [obj isKindOfClass:[NSArray class]];
}

- (NSString *)expand:(id)obj forSpec:(EZUriTemplateVariableSpec *)spec{
    if(spec.modifier == EZUriTemplateVariableModifierExploded || ![[self class] canExpandType:obj])
    {
        self.successor.delegate = self.delegate;
        return [self.successor expand:obj forSpec:spec];
    }
    
    __block NSMutableString *uri = [NSMutableString new];
    __block NSString* separtor = @"";
    
    NSArray* array = (NSArray*) obj;
    
    EZUriTemplateVariableSpec *unexplodedEmptyNameSpec = [EZUriTemplateVariableSpec new];
    unexplodedEmptyNameSpec.modifier = spec.modifier;
    unexplodedEmptyNameSpec.prefixLength = spec.prefixLength;
    
    EZVarNameAwareArrayExpander * __weak weakSelf = self;
    [array enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        if(![[weakSelf.delegate.characterMapExpander class] canExpandType:obj]){
            return;
        }
        
        NSString *value = [weakSelf.delegate.characterMapExpander expand:obj forSpec:unexplodedEmptyNameSpec];
        
        [uri appendFormat:@"%@%@", separtor, value];
        
        separtor = @",";
    }];
    
    NSString *var = [self.delegate.characterMapExpander expand:[EZUriCharacterMap new] forSpec:spec];

    if(![uri length]){
        return var;
    }
    
    // this is messy...
    NSString *sep = [var hasSuffix:@"="] ? @"" : @"=";
    
    return [NSString stringWithFormat:@"%@%@%@", var, sep, uri];
}
@end
