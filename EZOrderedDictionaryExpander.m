//
//  EZOrderedDictionaryExpander.m
//  uriTemplatesHost
//
//  Created by Donal Carpenter on 16/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import "EZOrderedDictionaryExpander.h"
#import "EZOrderedDictionary.h"

@implementation EZOrderedDictionaryExpander
+ (BOOL)canExpandType:(id)obj{
    return [obj isKindOfClass:[EZOrderedDictionary class]];
}

@end
