//
//  EZExpandableObjectExpander.m
//  uriTemplatesHost
//
//  Created by Donal Carpenter on 16/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import "EZExpandableObjectExpander.h"
#import "EZUriExpandableObject.h"

@implementation EZExpandableObjectExpander

@synthesize delegate, successor;

+ (BOOL)canExpandType:(id)obj{
    return [obj conformsToProtocol:@protocol(EZUriExpandableObject)];
}

- (NSString *)expand:(id)obj forSpec:(EZUriTemplateVariableSpec *)spec{
    if(![[self class] canExpandType:obj]){
        self.successor.delegate = self.delegate;
        return [self.successor expand:obj forSpec:spec];
    }
    
    id<EZUriExpandableObject> expando = (id<EZUriExpandableObject>) obj;
    
    return [expando expandForSpec:spec];
}
@end
