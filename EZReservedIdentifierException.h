//
//  EZReservedIdentifierException.h
//  uriTemplatesHost
//
//  Created by Donal Carpenter on 13/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EZReservedIdentifierException : NSException

@end
