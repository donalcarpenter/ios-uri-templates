//
//  EZVarNameAwareExplodedDictionaryExpander.h
//  uriTemplatesHost
//
//  Created by Donal Carpenter on 20/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EZUriTemplateVariableExpander.h"

@interface EZVarNameAwareExplodedDictionaryExpander : NSObject<EZUriTemplateVariableExpander>

@end
