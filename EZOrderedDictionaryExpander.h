//
//  EZOrderedDictionaryExpander.h
//  uriTemplatesHost
//
//  Created by Donal Carpenter on 16/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EZDictionaryExpander.h"

@interface EZOrderedDictionaryExpander : EZDictionaryExpander

@end
