//
//  EZUriTemplateProcessor.m
//  uriTemplates
//
//  Created by Donal Carpenter on 10/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import "EZUriTemplateProcessor.h"
#import "EZUriCharacter.h"
#import "EZUriCharacterMap.h"
#import "EZOrderedDictionary.h"
#import "EZExpression.h"

@interface EZUriTemplateProcessor()
-(void) processVariablesIntoUriChars: (NSDictionary*) variables ;
@end

@implementation EZUriTemplateProcessor
{
    NSMutableDictionary *_vars;
}

-(NSDictionary *)variables{
    return self->_vars;
}

- (id)initWithExpressions:(NSArray *)expressions andVariables:(NSDictionary *)variables{
    if(self = [super init]){
        
        // get copy of state
        self->_expressions = [expressions copy];
        [self processVariablesIntoUriChars: [variables copy]];
        
        return  self;
    }
    
    return nil;
}

// get each string and convert into URI Chars
- (void)processVariablesIntoUriChars: (NSDictionary*) variables{

    self->_vars = [NSMutableDictionary dictionary];
    
    NSMutableDictionary * __weak __vars = self->_vars;
    [variables enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        
        // try to understand what this is
        id var = [EZUriTemplateProcessor handleVariable:obj];
        
        // make sure we understood the thing being passed
        if(var)
        {
            [__vars setObject:var forKey:key];
        }
    
    }];
}

+ (id)handleVariable:(id)obj{
    
    // check if it is a simple string
    EZUriCharacterMap *map = [EZUriTemplateProcessor handleVariableAsString:obj];
    if(map)
        return map;
    
    // check if we're handling a dictionary
    NSDictionary *dict = [EZUriTemplateProcessor handleVariableAsDictionary:obj];
    if(dict)
        return dict;
    
    
    // check if this is a list/array
    NSArray *list = [EZUriTemplateProcessor handleVariableAsArray:obj];
    if(list)
        return list;
    
    // check if this is a custom object that handles it's own expansion
    id<EZUriExpandableObject> expando = [EZUriTemplateProcessor handleAsObject:obj];
    if(expando)
        return expando;
    
    // we don't understand what this is
    return nil;
}

+ (EZUriCharacterMap*) handleVariableAsString: (id) obj{
    if(![obj isKindOfClass:[NSString class]]){
        return nil;
    }
    
    return [[EZUriCharacterMap alloc] initWithString: (NSString*)obj];
}

+ (NSDictionary *) handleVariableAsDictionary: (id) obj{
    if(![obj isKindOfClass:[NSDictionary class]]){
        return nil;
    }
    
    NSDictionary* dict = (NSDictionary *) obj;
    
    // always use an ordered dictionary
    EZOrderedDictionary * res = [[EZOrderedDictionary alloc] init];
    
    [dict enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {

        id value = [EZUriTemplateProcessor handleVariable:obj];
        [res setValue:value forKey:key];
        
    }];
    
    return res;
}

+ (NSArray *) handleVariableAsArray: (id) obj{
    if(![obj isKindOfClass:[NSArray class]]){
        return nil;
    }
    
    NSMutableArray *array = [NSMutableArray array];
    NSArray *vars = (NSArray*) obj;
    
    [vars enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        [array addObject:[EZUriTemplateProcessor handleVariable:obj]];
        
    }];
    
    return array;
}

+ (id<EZUriExpandableObject>)handleAsObject:(id)obj{

    if(![obj conformsToProtocol: @protocol(EZUriExpandableObject)]){
        return nil;
    }
    
    return (id<EZUriExpandableObject>) obj;
}

-(NSString *)process{
    
    __block NSMutableString *processed = [NSMutableString new];

    [self.expressions enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        EZExpression *expr = (EZExpression*)obj;
        NSString *segment = [expr expand:self.variables];
        [processed appendString:segment];
        
    }];
    
    
    return [processed copy];
}
@end
