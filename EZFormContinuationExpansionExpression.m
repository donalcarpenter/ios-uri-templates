//
//  EZFormContinuationExpansionExpression.m
//  uriTemplates
//
//  Created by Donal Carpenter on 10/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import "EZFormContinuationExpansionExpression.h"

@implementation EZFormContinuationExpansionExpression

- (NSString *)prependor{
    return @"&";
}

@end
