//
//  EZConstantExpression.m
//  uriTemplates
//
//  Created by Donal Carpenter on 10/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import "EZConstantExpression.h"

@implementation EZConstantExpression

- (NSString *)expand:(NSDictionary *)variables{
    
    
    __block NSMutableString *constant = [NSMutableString new];
    
    [self.variableSpecs enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        EZUriTemplateVariableSpec * spec = (EZUriTemplateVariableSpec * )obj;
        
        [constant appendString:spec.name];
    }];
    
    return constant;
}

@end
