//
//  EZUriCharacter.m
//  uriTemplatesHost
//
//  Created by Donal Carpenter on 14/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import "EZUriCharacter.h"

@implementation EZUriCharacter
{
}

+ (EZUriCharacter*)uriCharacterWithCharCode:(unichar)charCode{
    return [[EZUriCharacter alloc] initWithCharCode:charCode];
}

-(id) initWithString:(NSString *)string{

    if([string length] < 1)
        return nil;
    
    unichar c = [string characterAtIndex:0];
    
    return [self initWithCharCode:c];
}

-(id) initWithPctEncodedString:(NSString *)pctEncodedString{
    
    // strip the % prefix
    uint prefixedWithSymbol = [pctEncodedString hasPrefix:@"%"];
    NSString *hexVal = [pctEncodedString substringFromIndex:prefixedWithSymbol];
    
    NSScanner *scan = [NSScanner scannerWithString:hexVal];
    unsigned int val;
    [scan scanHexInt:&val];
    
    unichar c = (unichar)val;
    EZUriCharacter *uchar = [self initWithCharCode:c];
    
    // need to make sure that reserved chars stay pct encoded
    if(uchar.charClass == EZUriCharacterClassReserved){
        uchar->_charClass = EZUriCharacterClassPctEncoded;
    }
    
    return uchar;
    
}

- (id)initWithCharCode:(unichar)charCode{
    self = [super init];
    if (self) {
        self->_charCode = charCode;
        
        // perform map
        [self evaluateCharacterClassFromUnicode];
        
        return self;
    }
    return nil;
}

- (NSString *)string{
    if(self.charClass == EZUriCharacterClassUnreserved)
        return [NSString stringWithFormat:@"%C", self.charCode];
    
    return [NSString stringWithFormat:@"%%%X", self.charCode];
}

- (NSString *)reservedString{
    if(self.charClass == EZUriCharacterClassPctEncoded)
        return [NSString stringWithFormat:@"%%%X", self.charCode];
    
    return [NSString stringWithFormat:@"%C", self.charCode];
}

- (void) evaluateCharacterClassFromUnicode{
    
    // check for standard alpha num ranges
    if(
            (self->_charCode >= 'a' && self->_charCode <= 'z')
       ||   (self->_charCode >= 'A' && self->_charCode <= 'Z')
       ||   (self->_charCode >= '0' && self->_charCode <= '9')
         ){
        self->_charClass = EZUriCharacterClassUnreserved;
        return;
    }
    
    // this is a special digit so treat individually
    switch (self.charCode) {
        case '-':
        case '.':
        case '_':
        case '~':
            self->_charClass = EZUriCharacterClassUnreserved;
            break;
            
        case ':':
        case '/':
        case '?':
        case '#':
        case '[':
        case ']':
        case '@':
        case '!':
        case '$':
        case '&':
        case '\'':
        case '(':
        case ')':
        case '*':
        case '+':
        case ',':
        case ';':
        case '=':
            
            self->_charClass = EZUriCharacterClassReserved;
            break;
            
        default:
            self->_charClass = EZUriCharacterClassPctEncoded;
            break;
    }
}

@end
