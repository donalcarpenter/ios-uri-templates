//
//  EZDictionaryExpander.m
//  uriTemplatesHost
//
//  Created by Donal Carpenter on 16/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import "EZDictionaryExpander.h"

@implementation EZDictionaryExpander

@synthesize delegate = _delegate, successor;

// check if it's a dictionary
+ (BOOL)canExpandType:(id)obj{
    return [obj isKindOfClass:[NSDictionary class]];
}

-(NSString *) expand: (id) obj forSpec: (EZUriTemplateVariableSpec *) spec{
    
    
    // if we cannot process this item then
    // pass to the next handler in the chain
    if(![[self class] canExpandType:obj])
    {
        self.successor.delegate = self.delegate;
        return [self.successor expand:obj forSpec:spec];
    }
    
    // we know this is a dictionary from the above check
    NSDictionary* dictionary = (NSDictionary*)obj;
    
    if(![dictionary count]){
        // we have nothing in this dictionary so return nil
        // to denote that it is not an empty string
        return nil;
    }
    
    __block NSMutableString *uri = [NSMutableString new];
    __block NSString *separator = @"";
    
    
    EZDictionaryExpander * __weak weakSelf = self;
    
    // get the name/value separator from the delegate
    NSString *nameValueSeparator = [self.delegate.expression keyValueSeparatorForSpec:spec];
    
    [dictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        
        // if this is not something we can process then ignore
        if(![[weakSelf.delegate.characterMapExpander class] canExpandType:obj]){
            return;
        }
        
        // pass to the delegae to process the char map and return a string
        NSString *value = [self.delegate.characterMapExpander expand:obj forSpec:spec];
        
        [uri appendFormat:@"%@%@%@%@", separator, key, nameValueSeparator, value];
        
        separator = [self.delegate.expression separatorForSpec:spec];
        
    }];
    
    return uri;
}
@end
