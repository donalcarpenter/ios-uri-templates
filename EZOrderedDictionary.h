//
//  EZOrderedDictionary.h
//  uriTemplatesHost
//
//  Created by Donal Carpenter on 15/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EZOrderedDictionary : NSMutableDictionary

- (void) insertObject:(id) anObject atIndex: (uint) index forKey: (id)key;

- (id) objectAtIndex: (uint) index;

@end
