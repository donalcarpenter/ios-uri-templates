//
//  EZVarNameAwareExplodedDictionaryExpander.m
//  uriTemplatesHost
//
//  Created by Donal Carpenter on 20/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import "EZVarNameAwareExplodedDictionaryExpander.h"

@implementation EZVarNameAwareExplodedDictionaryExpander

@synthesize delegate, successor;

+ (BOOL)canExpandType:(id)obj{
    return [obj isKindOfClass:[NSDictionary class]];
}

- (NSString *)expand:(id)obj forSpec:(EZUriTemplateVariableSpec *)spec{
    if(spec.modifier != EZUriTemplateVariableModifierExploded || ![[self class] canExpandType:obj])
    {
        self.successor.delegate = self.delegate;
        return [self.successor expand:obj forSpec:spec];
    }
    
    __block NSMutableString *uri = [NSMutableString new];
    __block NSString *separtor = @"";
    
    NSDictionary* dict = (NSDictionary*) obj;
    
    EZUriTemplateVariableSpec *itemSpec = [EZUriTemplateVariableSpec new];
    itemSpec.modifier = spec.modifier;
    itemSpec.prefixLength = spec.prefixLength;
    
    
    EZVarNameAwareExplodedDictionaryExpander * __weak weakSelf= self;
    [dict enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        if(![[weakSelf.delegate.characterMapExpander class] canExpandType:obj]){
            return;
        }
        
        itemSpec.name = key;
        
        NSString *value = [weakSelf.delegate.characterMapExpander expand:obj forSpec:itemSpec];
        
        [uri appendFormat:@"%@%@", separtor, value];
        
        separtor = [weakSelf.delegate.expression separatorForSpec:spec];
    }];
    
    
    return uri;
}
@end
