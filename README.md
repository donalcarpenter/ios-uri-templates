This is a complete implementation of the [Uri Template Specification](http://tools.ietf.org/html/rfc6570).

Unlike other implementation this has no external dependencies.

To see it in action run the unit tests


