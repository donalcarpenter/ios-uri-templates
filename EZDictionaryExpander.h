//
//  EZDictionaryExpander.h
//  uriTemplatesHost
//
//  Created by Donal Carpenter on 16/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//
//  The EZDictionaryExpander is responsible for
//  enumerating through dictionar objects. It relies
//  heavily on an delegate to convert EZUriCharacterMaps
//  into NSStrings and provide the chars that separate
//  elements and name/vaue paire
//
//

#import <Foundation/Foundation.h>
#import "EZUriTemplateVariableExpander.h"



@interface EZDictionaryExpander : NSObject<EZUriTemplateVariableExpander>

@end
