//
//  EZUriTemplateProcessor.h
//  uriTemplates
//
//  Created by Donal Carpenter on 10/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EZUriCharacterMap.h"
#import "EZUriExpandableObject.h"

@protocol EZUriTemplateVariableHandler <NSObject>

+ (id) handleVariable: (id) obj;

+ (EZUriCharacterMap*) handleVariableAsString: (id) obj;

+ (NSDictionary *) handleVariableAsDictionary: (id) obj;

+ (NSArray *) handleVariableAsArray: (id) obj;

+ (id<EZUriExpandableObject>) handleAsObject: (id) obj;

@end

@interface EZUriTemplateProcessor : NSObject<EZUriTemplateVariableHandler>

@property (readonly) NSDictionary* variables;
@property (readonly) NSArray* expressions;

- (id) initWithExpressions: (NSArray *) expressions andVariables: (NSDictionary*) variables;

- (NSString *) process;

@end
