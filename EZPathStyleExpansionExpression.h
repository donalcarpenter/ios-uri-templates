//
//  EZPathStyleExpansionExtension.h
//  uriTemplatesHost
//
//  Created by Donal Carpenter on 10/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import "EZExpression.h"

// http://tools.ietf.org/html/rfc6570#section-3.2.7
@interface EZPathStyleExpansionExpression : EZExpression

@end
