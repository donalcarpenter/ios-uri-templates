//
//  EZUriExpandableObject.h
//  uriTemplatesHost
//
//  Created by Donal Carpenter on 14/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EZUriTemplateVariableSpec.h"

@protocol EZUriExpandableObject <NSObject>
- (NSString *) expandForSpec: (EZUriTemplateVariableSpec*) spec;
@end
