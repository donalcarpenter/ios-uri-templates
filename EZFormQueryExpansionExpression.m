//
//  EZFormQueryExpansionExpression.m
//  uriTemplates
//
//  Created by Donal Carpenter on 10/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import "EZFormQueryExpansionExpression.h"

@implementation EZFormQueryExpansionExpression

-(NSString *)prependor{
    return @"?";
}

- (NSString *)separatorForSpec:(EZUriTemplateVariableSpec *)variableSpec{
    return @"&";
}

- (BOOL)applySeparatorWhenExpandedVariableIsEmpty{
    return YES;
}
@end
