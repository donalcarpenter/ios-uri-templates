//
//  EZOrderedDictionary.m
//  uriTemplatesHost
//
//  Created by Donal Carpenter on 15/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import "EZOrderedDictionary.h"

@implementation EZOrderedDictionary
{
    NSMutableDictionary *_dictionary;
    NSMutableOrderedSet *_index;
}

// https://developer.apple.com/library/mac/documentation/Cocoa/Reference/Foundation/Classes/NSMutableDictionary_Class/Reference/Reference.html
#pragma mark primitive methods

- (id)init
{
    self = [super init];
    if (self) {
        self->_dictionary = [NSMutableDictionary dictionary];
        self->_index = [NSMutableOrderedSet orderedSet];
    }
    return self;
}

- (instancetype)initWithObjects:(const id [])objects forKeys:(const id<NSCopying> [])keys count:(NSUInteger)cnt{
    
    if(self = [super init]){
        self->_dictionary = [[NSMutableDictionary alloc] initWithObjects:objects forKeys:keys count:cnt];
        self->_index = [[NSMutableOrderedSet alloc] initWithObjects:keys count:cnt];
    }
    return self;
}

- (instancetype) initWithObjects:(NSArray *)objects forKeys:(NSArray *)keys{
    if(self = [super init]){
        self->_dictionary = [[NSMutableDictionary alloc] initWithObjects:objects forKeys:keys];
        self->_index = [NSMutableOrderedSet orderedSetWithArray:keys];
    }
    return self;
}

- (void) insertObject:(id) anObject atIndex: (uint) index forKey: (id)key{
    
    [self->_index insertObject:key atIndex:index];
    [self->_dictionary setObject:anObject forKey:key];
    
}

- (void)setObject:(id)anObject forKey:(id<NSCopying>)aKey{
    [self->_dictionary setObject:anObject forKey:aKey];
    [self->_index addObject:aKey];
}

-(void)removeObjectForKey:(id)aKey{
    [self->_dictionary removeObjectForKey:aKey];
    [self->_index removeObject:aKey];
}

- (NSUInteger)count{
    return [self->_index count];
}

- (id)objectForKey:(id)aKey{
    return [self->_dictionary objectForKey:aKey];
}

- (id)objectAtIndex: (uint) index{
    return self->_dictionary[[self->_index objectAtIndex:index]];
}

- (NSEnumerator *)keyEnumerator{
    return [self->_index objectEnumerator];
}

- (void)enumerateKeysAndObjectsUsingBlock:(void (^)(id, id, BOOL *))block{
    for (id key in self->_index) {
        id obj = self->_dictionary[key];
        BOOL stop = NO;
        
        block(key, obj, &stop);
        
        if(stop){
            break;
        }
    }
}

@end
