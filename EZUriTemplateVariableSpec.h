//
//  EZUriTemplateVariable.h
//  uriTemplates
//
//  Created by Donal Carpenter on 10/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, EZUriTemplateVariableModifier) {
    EZUriTemplateVariableModifierNone,
    EZUriTemplateVariableModifierExploded,
    EZUriTemplateVariablePrefixed,
};

@interface EZUriTemplateVariableSpec : NSObject
@property EZUriTemplateVariableModifier modifier;
@property uint prefixLength;
@property (strong) NSString* name;

-(id) initWithName: (NSString*)name;
@end
