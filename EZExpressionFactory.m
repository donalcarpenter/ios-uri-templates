//
//  EZExpressionFactory.m
//  uriTemplates
//
//  Created by Donal Carpenter on 10/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import "EZExpressionFactory.h"
#import "EZSimpleStringExpansionExpression.h"
#import "EZReservedStringExpansionExpression.h"
#import "EZFragmentExpansionExpression.h"
#import "EZLabelExpansionExpression.h"
#import "EZPathSegmentExpansionExpression.h"
#import "EZFormQueryExpansionExpression.h"
#import "EZFormContinuationExpansionExpression.h"
#import "EZConstantExpression.h"
#import "EZPathStyleExpansionExpression.h"
#import "EZProhibitedIdentifierException.h"
#import "EZReservedIdentifierException.h"


@implementation EZExpressionFactory
{
}

- (EZExpression *)expressionForExpansion:(NSString *)expansionIdentifier {
    
    if(self->_inExpression)
    {
        if([expansionIdentifier isEqualToString:@"+"])
        return [EZReservedStringExpansionExpression new];
        
        if([expansionIdentifier isEqualToString:@"#"])
        return [EZFragmentExpansionExpression new];
        
        if([expansionIdentifier isEqualToString:@"."])
        return [EZLabelExpansionExpression new];
        
        if([expansionIdentifier isEqualToString:@"/"])
        return [EZPathSegmentExpansionExpression new];
        
        if([expansionIdentifier isEqualToString:@"?"])
        return [EZFormQueryExpansionExpression new];
        
        if([expansionIdentifier isEqualToString:@"&"])
        return [EZFormContinuationExpansionExpression new];
        
        if([expansionIdentifier isEqualToString:@";"])
        return [EZPathStyleExpansionExpression new];
        
        // check for prohibited and reserved identifiers
        if([@"$()" rangeOfString:expansionIdentifier].location != NSNotFound){
            @throw [EZProhibitedIdentifierException new];
        }
        
        if([@"=,!@|" rangeOfString:expansionIdentifier].location != NSNotFound){
            @throw [EZReservedIdentifierException new];
        }
        
        return  [[EZSimpleStringExpansionExpression alloc] init];
    }else
    {
        return  [[EZConstantExpression alloc] init];
    }
    
}
@end
