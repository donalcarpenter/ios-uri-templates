//
//  EZUriTemplateVariableExpanderFactory.h
//  uriTemplatesHost
//
//  Created by Donal Carpenter on 16/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EZUriTemplateVariableExpander.h"
#import "EZUriTemplateVariableSpec.h"

@protocol EZUriTemplateVariableExpander;

@interface EZUriTemplateVariableExpanderFactory : NSObject;

@property (weak, readonly) id<EZUriTemplateVariableExpander> characterMapExpander;

+ (id<EZUriTemplateVariableExpander>) convertExpanderListToChain: (NSArray*) expanders;

+ (id<EZUriTemplateVariableExpander>) defaultExpanderChain;

- (id)initWithCharacterMapExpander: (id<EZUriTemplateVariableExpander>) characterMapExpander;

- (id<EZUriTemplateVariableExpander>) expanderForObject: (id) obj;

@end
