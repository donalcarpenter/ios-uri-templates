//
//  EZVarNameAwareDictionaryExpander.m
//  uriTemplatesHost
//
//  Created by Donal Carpenter on 20/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import "EZVarNameAwareDictionaryExpander.h"

@implementation EZVarNameAwareDictionaryExpander

@synthesize delegate, successor;

+ (BOOL)canExpandType:(id)obj{
    return [obj isKindOfClass:[NSDictionary class]];
}

- (NSString *)expand:(id)obj forSpec:(EZUriTemplateVariableSpec *)spec{
    if(spec.modifier == EZUriTemplateVariableModifierExploded || ![[self class] canExpandType:obj])
    {
        self.successor.delegate = self.delegate;
        return [self.successor expand:obj forSpec:spec];
    }
    
    __block NSMutableString *uri = [NSMutableString new];
    __block NSString *separtor = @"";
    
    NSDictionary* dict = (NSDictionary*) obj;
    
    NSString *keyValueSeparator = @",";
    
    EZUriTemplateVariableSpec *unexplodedEmptyNameSpec = [EZUriTemplateVariableSpec new];
    unexplodedEmptyNameSpec.modifier = spec.modifier;
    unexplodedEmptyNameSpec.prefixLength = spec.prefixLength;
    
    EZVarNameAwareDictionaryExpander * __weak weakSelf = self;
    
    [dict enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        if(![[weakSelf.delegate.characterMapExpander class] canExpandType:obj]){
            return;
        }

        NSString *name = [weakSelf.delegate.characterMapExpander expand:[[EZUriCharacterMap alloc] initWithString:key] forSpec:unexplodedEmptyNameSpec];
        NSString *value = [weakSelf.delegate.characterMapExpander expand:obj forSpec:unexplodedEmptyNameSpec];
        
        if([value length] == 0){
            [uri appendFormat:@"%@%@", separtor, name];
        }
        
        else if(![name length]){
            [uri appendFormat:@"%@%@", separtor, value];
        }
        
        else{
            [uri appendFormat:@"%@%@%@%@", separtor, name, keyValueSeparator, value];
        }
        
        // all unexploded lists use , as item separator
        separtor = @",";
    }];
    
    NSString *var = [self.delegate.characterMapExpander expand:[EZUriCharacterMap new] forSpec:spec];
    
    if(![uri length]){
        return var;
    }
    
    NSString *sep = [var hasSuffix:@"="] ? @"" : @"=";
    
    return [NSString stringWithFormat:@"%@%@%@", var, sep, uri];
    
}
@end
