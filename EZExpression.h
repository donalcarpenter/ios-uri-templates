//
//  EXExpression.h
//  uriTemplates
//
//  Created by Donal Carpenter on 10/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EZUriTemplateVariableSpec.h"
#import "EZUriExpandableObject.h"
#import "EZUriCharacterMap.h"


@protocol EZUriTemplateVariableExpanderDelegate;
@protocol EZUriTemplateVariableExpander;

@interface EZExpression : NSObject 
{
    @protected NSMutableArray* _variableSpecs;
}

#define RESERVED_URI_CHARS = @":/?#[]@!$&'()*+,;="

@property (readonly) NSArray* variableSpecs;

- (void) addVariableSpec: (EZUriTemplateVariableSpec*) variableSpec;

- (NSString *) separatorForSpec: (EZUriTemplateVariableSpec*) variableSpec;

- (NSString *) keyValueSeparatorForSpec: (EZUriTemplateVariableSpec*) variableSpec;

- (NSString *) prependor;

- (NSString *) expand: (NSDictionary*)variables;

- (BOOL) applySeparatorWhenExpandedVariableIsEmpty;

@end
