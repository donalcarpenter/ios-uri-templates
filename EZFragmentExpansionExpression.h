//
//  EZFragmentExpansionExpression.h
//  uriTemplates
//
//  Created by Donal Carpenter on 10/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EZReservedStringExpansionExpression.h"

// http://tools.ietf.org/html/rfc6570#section-3.2.4
@interface EZFragmentExpansionExpression : EZReservedStringExpansionExpression

@end
