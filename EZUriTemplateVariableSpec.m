//
//  EZUriTemplateVariable.m
//  uriTemplates
//
//  Created by Donal Carpenter on 10/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import "EZUriTemplateVariableSpec.h"

@implementation EZUriTemplateVariableSpec
- (id)init
{
    self = [super init];
    if (self) {
        self->_modifier = EZUriTemplateVariableModifierNone;
        return self;
    }
    return nil;
}

-(id)initWithName:(NSString *)name{
    
    self = [self init];
    if(self)
        self->_name = name;
    
    return self;
}
@end
