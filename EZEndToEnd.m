//
//  EZEndToEnd.m
//  uriTemplatesHost
//
//  Created by Donal Carpenter on 21/01/2014.
//  Copyright (c) 2014 ezetop. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "EZUriTemplateParser.h"
#import "EZUriTemplateProcessor.h"
#import "EZOrderedDictionary.h"

@interface EZEndToEnd : XCTestCase

@end

@implementation EZEndToEnd
{
    NSDictionary *vars;
}

- (void)setUp
{
    [super setUp];
    
    
    self->vars = @{
                   @"count"  : @[@"one", @"two", @"three"],
                   @"dom"    : @[@"example", @"com"],
                   @"dub"    : @"me/too",
                   @"hello"  : @"Hello World!",
                   @"half"   : @"50%",
                   @"var"    : @"value",
                   @"who"    : @"fred",
                   @"base"   : @"http://example.com/home/",
                   @"path"   : @"/foo/bar",
                   @"list"   : @[@"red", @"green", @"blue"],
                   @"keys"   : [[EZOrderedDictionary alloc] initWithObjects:@[@";", @".", @","] forKeys:@[@"semi", @"dot", @"comma"]],
                   @"v"      : @"6",
                   @"x"      : @"1024",
                   @"y"      : @"768",
                   @"empty"  : @"",
                   @"empty_keys"   : @{},
                   @"undef"  : [NSNull null]
                   };
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}


- (NSString *) executeTemplate: (NSString *) template{
    EZUriTemplateParser *parse = [[EZUriTemplateParser alloc] initWithTemplate:template];
    NSError *e;
    
    NSArray *components = [parse expressions:&e];
    
    EZUriTemplateProcessor *proc = [[EZUriTemplateProcessor alloc] initWithExpressions:components andVariables:vars];
    
    return [proc process];
}

- (void)testExample
{
    
    NSString *template = @"{var}";
    NSString *expected = @"value";
    
    NSString *expanded = [self executeTemplate:template];
    
    XCTAssertEqualObjects(expected, expanded, @"'%@' expanded to '%@'", template, expanded);

}

- (void)testExample2
{
    
    NSString *template = @"O{var}X";
    NSString *expected = @"OvalueX";
    
    NSString *expanded = [self executeTemplate:template];
    
    XCTAssertEqualObjects(expected, expanded, @"'%@' expanded to '%@'", template, expanded);
    
}

-(void) testAllTestCases_This_Is_The_Big_Kahuna{
    
    NSString *path = [[NSBundle mainBundle]pathForResource:@"TestCases" ofType:@"plist"];
    NSArray *arr = [NSArray arrayWithContentsOfFile:path];
    
    XCTAssertNotNil(arr, @"could not load test cases");
    
    
    EZEndToEnd * __weak weakSelf = self;
    [arr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {

        NSArray *test = (NSArray *) obj;
        
        NSString *template = test[0];
        NSString *expected = test[1];
        
        NSString *expanded = [weakSelf executeTemplate:template];
        
        XCTAssertEqualObjects(expected, expanded, @"'%@' expanded to '%@', expected: '%@'", template, expanded, expected);
        
    }];
}

@end
